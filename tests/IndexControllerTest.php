<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Controllers\IndexController;
use Sendpulse\Http\Request;
use Sendpulse\Http\StatusCode;

class IndexControllerTest extends TestCase
{
    public function testIndexMethod()
    {
        $request = new Request();
        $controller = new IndexController();
        $response = $controller->index($request);
        $content = strtolower(trim($response->getData()));
        $this->assertEquals(StatusCode::OK, $response->getStatus());
        $this->assertStringEndsWith('</html>', $content);
    }

    public function testIndexMethodAjaxRequest()
    {
        $_SERVER['HTTP_X_REQUESTED_WITH'] = 'xmlhttprequest';
        $request = new Request();
        $controller = new IndexController();
        $response = $controller->index($request);
        $this->assertTrue($request->isAjax());
        $this->assertEquals(StatusCode::NOT_FOUND, $response->getStatus());
    }
}