<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Controllers\EventController;
use Sendpulse\Http\Request;
use Sendpulse\Http\StatusCode;
use Services\Event;

class EventControllerTest extends TestCase
{
    public function testIndex()
    {
        $request = new Request();
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['list'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('list')->withAnyParameters()->willReturn(['list' => [], 'count' => 0]);

        $response = $controller->index($request, $mock);
        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }

    public function testCreateSuccess()
    {
        $date = new \DateTime();

        $_REQUEST = [
            'date' => $date->modify('+1 hours')->format('Y-m-d H:i:s'),
            'title' => 'Event title',
            'description' => 'Event description'
        ];

        $request = new Request();
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('create')->withAnyParameters()->willReturn(null);

        $response = $controller->create($request, $mock);
        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }

    public function testCreateFailedDate()
    {
        $date = new \DateTime();

        $_REQUEST = [
            'date' => $date->modify('-1 hours')->format('Y-m-d H:i:s'),
            'title' => 'Event title',
            'description' => 'Event description'
        ];

        $request = new Request();
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('create')->withAnyParameters()->willReturn(null);

        $response = $controller->create($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testCreateFailedData()
    {
        $date = new \DateTime();

        $_REQUEST = [
            'date' => $date->modify('+1 hours')->format('Y-m-d H:i:s'),
            'description' => 'Event description'
        ];

        $request = new Request();
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('create')->withAnyParameters()->willReturn(null);

        $response = $controller->create($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testUpdateSuccess()
    {
        $date = new \DateTime();

        $_REQUEST = [
            'date' => $date->modify('+1 hours')->format('Y-m-d H:i:s'),
            'title' => 'Event title',
            'description' => 'Event description'
        ];

        $request = new Request(['id' => 1]);
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('update')->withAnyParameters()->willReturn(null);

        $response = $controller->update($request, $mock);
        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }

    public function testUpdateFailedDate()
    {
        $date = new \DateTime();

        $_REQUEST = [
            'date' => $date->modify('- 1 days')->format('Y-m-d H:i:s'),
            'title' => 'Event title',
            'description' => 'Event description'
        ];

        $request = new Request(['id' => 1]);
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('update')->withAnyParameters()->willReturn(null);

        $response = $controller->update($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testUpdateFailedData()
    {
        $date = new \DateTime();

        $_REQUEST = [
            'date' => $date->modify('+ 1 days')->format('Y-m-d H:i:s'),
            'title' => 'Event title',
        ];

        $request = new Request(['id' => 1]);
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('update')->withAnyParameters()->willReturn(null);

        $response = $controller->update($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testComplete()
    {
        $request = new Request(['id' => 1]);
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['complete'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('complete')->withAnyParameters()->willReturn(null);

        $response = $controller->complete($request, $mock);
        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }

    public function testDestroy()
    {
        $request = new Request(['id' => 1]);
        $controller = new EventController();
        $controller->user = (object)['id' => 1];

        $mock = $this->getMockBuilder(Event::class)
            ->setMethods(['destroy'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('destroy')->withAnyParameters()->willReturn(null);

        $response = $controller->destroy($request, $mock);
        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }
}