<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Sendpulse\Http\Request;
use Services\Validators;

class ValidatorsTest extends TestCase
{
    public function testLoginUserValidData()
    {
        $email = 'test@mail.com';
        $password = 'password';
        $_REQUEST = ['email' => $email, 'password' => $password];
        $request = new Request();
        list($errors,) = Validators::loginUser($request);
        $this->assertFalse($errors);
    }

    public function testLoginUserInvalidEmail()
    {
        $email = 'test#mail.com';
        $password = 'password';
        $_REQUEST = ['email' => $email, 'password' => $password];
        $request = new Request();
        list($errors,) = Validators::loginUser($request);
        $this->assertEquals(1, count($errors));
    }

    public function testLoginUserInvalidPassword()
    {
        $email = 'test@mail.com';
        $_REQUEST = ['email' => $email];
        $request = new Request();
        list($errors,) = Validators::loginUser($request);
        $this->assertEquals(1, count($errors));
    }

    public function testCreateUserValidData()
    {
        $email = 'usertest@gmail.com';
        $password = 'Hol2RetHet';
        $_REQUEST = ['email' => $email, 'password' => $password, 'confirmPassword' => $password];
        $request = new Request();
        list($errors,) = Validators::createUser($request);
        $this->assertFalse($errors);
    }

    public function testCreateUserInvalidEmail()
    {
        $email = 'usert%est@gmail.com';
        $password = 'Hol2RetHet';
        $_REQUEST = ['email' => $email, 'password' => $password, 'confirmPassword' => $password];
        $request = new Request();
        list($errors,) = Validators::createUser($request);
        $this->assertEquals(1, count($errors));
    }

    public function testCreateUserInvalidConfirmPassword()
    {
        $email = 'usert%est@gmail.com';
        $password = 'Hol2RetHet';
        $confirmPassword = 'failedpassword';
        $_REQUEST = ['email' => $email, 'password' => $password, 'confirmPassword' => $confirmPassword];
        $request = new Request();
        list($errors,) = Validators::createUser($request);
        $this->assertEquals(1, count($errors));
    }

    public function testCreateUserInvalidData()
    {
        $_REQUEST = [];
        $request = new Request();
        list($errors) = Validators::createUser($request);
        $this->assertEquals(8, count($errors));
    }

    public function testSetEventValidData()
    {
        $currentDate = new \DateTime();

        $_REQUEST = [
            'date' => $currentDate->modify('+1 hours')->format('Y-m-d H:i:s'),
            'title' => 'Test title',
            'description' => 'Test description text'
        ];

        $request = new Request();
        list($errors,) = Validators::setEvent($request);
        $this->assertFalse($errors);
    }

    public function testSetEventInvalidData()
    {
        $currentDate = new \DateTime();

        $_REQUEST = [
            'date' => $currentDate->modify('-1 hours')->format('Y-m-d H:i:s'),
            'title' => 'Test title',
            'description' => 'Test description text'
        ];

        $request = new Request();
        list($errors,) = Validators::setEvent($request);
        $this->assertEquals(1, count($errors));
    }

    public function testSetEventInvalidTitle()
    {
        $currentDate = new \DateTime();

        $_REQUEST = [
            'date' => $currentDate->modify('+1 hours')->format('Y-m-d H:i:s'),
            'description' => 'Test description text'
        ];

        $request = new Request();
        list($errors,) = Validators::setEvent($request);
        $this->assertEquals(2, count($errors));
    }

    public function testSetEventInvalidDescription()
    {
        $currentDate = new \DateTime();

        $_REQUEST = [
            'date' => $currentDate->modify('+1 hours')->format('Y-m-d H:i:s'),
            'title' => 'Test title'
        ];

        $request = new Request();
        list($errors,) = Validators::setEvent($request);
        $this->assertEquals(2, count($errors));
    }

    public function testPaginateDefaultValues()
    {
        $_REQUEST = [];
        $request = new Request();
        $data = Validators::paginate($request);

        $this->assertEquals('id', $data['sortField']);
        $this->assertEquals('desc', $data['sortDirection']);
        $this->assertEquals(10, $data['limit']);
        $this->assertEquals(0, $data['offset']);
        $this->assertEquals('', $data['searchText']);
        $this->assertEquals([], $data['searchFields']);
        $this->assertEquals([], $data['filters']);
    }

    public function testPaginateNewValues()
    {
        $_REQUEST = [
            'limit' => 20,
            'offset' => 50,
            'sortField' => 'date',
            'sortDirection' => 'asc',
            'searchText' => 'text',
            'searchFields' => ['title', 'desc'],
            'filters' => []

        ];
        $request = new Request();
        $data = Validators::paginate($request);

        $this->assertEquals('date', $data['sortField']);
        $this->assertEquals('asc', $data['sortDirection']);
        $this->assertEquals(20, $data['limit']);
        $this->assertEquals(50, $data['offset']);
        $this->assertEquals('text', $data['searchText']);
        $this->assertEquals(['title', 'desc'], $data['searchFields']);
        $this->assertEquals([], $data['filters']);
    }

}