<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Controllers\UserController;
use Sendpulse\Http\Request;
use Sendpulse\Http\StatusCode;
use Services\User;

class UserControllerTest extends TestCase
{
    public function testCreateSuccess()
    {
        $_REQUEST = [
            'email' => 'testuser@mail.com',
            'password' => 'password',
            'confirmPassword' => 'password'
        ];

        $request = new Request();
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['getFromEmail', 'create'])
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('getFromEmail')->withAnyParameters()->willReturn(false);
        $mock->method('create')->withAnyParameters()->willReturn(false);

        $response = $controller->create($request, $mock);
        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }

    public function testCreateFailedEmail()
    {
        $_REQUEST = [
            'email' => '@mail.com',
            'password' => 'password',
            'confirmPassword' => 'password'
        ];

        $request = new Request();
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['getFromEmail', 'create'])
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('getFromEmail')->withAnyParameters()->willReturn(false);
        $mock->method('create')->withAnyParameters()->willReturn(false);

        $response = $controller->create($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testCreateFailedPassword()
    {
        $_REQUEST = [
            'email' => '@mail.com',
            'password' => 'password_one',
            'confirmPassword' => 'password_two'
        ];

        $request = new Request();
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['getFromEmail', 'create'])
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('getFromEmail')->withAnyParameters()->willReturn(false);
        $mock->method('create')->withAnyParameters()->willReturn(false);

        $response = $controller->create($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testCreateEmailAlreadyExists()
    {
        $_REQUEST = [
            'email' => '@mail.com',
            'password' => 'password',
            'confirmPassword' => 'password'
        ];

        $request = new Request();
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['getFromEmail', 'create'])
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('getFromEmail')->withAnyParameters()->willReturn(true);
        $mock->method('create')->withAnyParameters()->willReturn(false);

        $response = $controller->create($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testLoginSuccess()
    {
        $_REQUEST = [
            'email' => 'user@mail.com',
            'password' => 'password'
        ];

        $request = new Request();
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['login', 'auth'])
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('login')->withAnyParameters()->willReturn(null);
        $mock->method('auth')->withAnyParameters()->willReturn(true);

        $response = $controller->login($request, $mock);
        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }

    public function testLoginFailed()
    {
        $_REQUEST = [
            'email' => 'user@mail.com',
            'password' => 'password'
        ];

        $request = new Request();
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['login', 'auth'])
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('login')->withAnyParameters()->willReturn(null);
        $mock->method('auth')->withAnyParameters()->willReturn(false);

        $response = $controller->login($request, $mock);
        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testLoginFailedEmail()
    {
        $_REQUEST = [
            'password' => 'password'
        ];

        $request = new Request();
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['login', 'auth'])
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('login')->withAnyParameters()->willReturn(null);
        $mock->method('auth')->withAnyParameters()->willReturn(true);

        $response = $controller->login($request, $mock);

        $this->assertEquals(StatusCode::BAD_REQUEST, $response->getStatus());
    }

    public function testLogout()
    {
        $controller = new UserController();

        $mock = $this->getMockBuilder(User::class)
            ->setMethods(['logout'])
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('logout')->withAnyParameters()->willReturn(null);

        $response = $controller->logout($mock);

        $this->assertEquals(StatusCode::OK, $response->getStatus());
    }
}