<?php

use Config\System;
use Sendpulse\App;
use RedBeanPHP\R;
use Sendpulse\Http\Session;
use Sendpulse\Http\Request;
use Sendpulse\Http\Response;
use Sendpulse\Http\StatusCode;
use Sendpulse\Service\Log;

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

define('BASE_PATH', __DIR__);

require_once BASE_PATH . '/vendor/autoload.php';

Log::init(BASE_PATH.'/error.log');

set_error_handler('errorHandler', E_ALL);
set_exception_handler('exceptionHandler');

try {

    Session::init(System::SESSION_LIFETIME);
    date_default_timezone_set(System::DEFAULT_TIMEZONE);

    $dsn = 'mysql:host=' . System::MYSQL_HOST . '; port=' . System::MYSQL_PORT . '; dbname=' . System::MYSQL_DATABASE;
    R::setup($dsn, System::MYSQL_USER, System::MYSQL_PASSWORD);
    R::freeze(true);
    App::run(new Request());
} catch (Throwable $exc) {
    Response::status('Operation Failed.', StatusCode::BAD_REQUEST)->run();
    Log::send()->error($exc->getMessage(), $exc->getTrace());
} finally {
    R::close();
}

function exceptionHandler(Throwable $exc)
{
    Log::send()->error($exc->getMessage(), $exc->getTrace());
    Response::status('Operation Failed.', StatusCode::BAD_REQUEST)->run();
}

function errorHandler(int $errno, string $errstr, string $errfile, int $errline)
{
    Log::send()->error($errstr . '. Number: ' . $errno . ', File: ' . $errfile . ', Line: ' . $errline);
    Response::status('Operation Failed.', StatusCode::BAD_REQUEST)->run();
}
