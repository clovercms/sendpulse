'use strict';

var gulp = require('gulp');
var gulpWebpack = require('gulp-webpack');
var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var concatCss = require('gulp-concat-css');
var minifyCSS = require('gulp-minify-css');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var modifyCssUrls = require('gulp-modify-css-urls');
var merge = require('merge-stream');

var config = require('./config/config.json');

gulp.task('webpack:app:dev', function () {
    return gulp.src('')
        .pipe(gulpWebpack(require('./config/webpack.dev')), webpack)
        .pipe(gulp.dest('../public'));
});

gulp.task('webpack:app:dev:watch', function () {
    return gulp.src('')
        .pipe(gulpWebpack(webpackMerge(require('./config/webpack.dev'), {
            watch: true //,
            // watchOptions: {
            //     aggreagateTimeout: 100
            // }
        })), webpack)
        .pipe(gulp.dest('../public'));
});

gulp.task('webpack:app:prod', function () {
    return gulp.src('')
        .pipe(gulpWebpack(require('./config/webpack.prod')), webpack)
        .pipe(gulp.dest('../public'));
});

gulp.task('concat:vendor:js', function () {
    return gulp.src(config.javascripts.vendor)
        .pipe(concat('vendor.ext.js', {newLine: ';'}))
        .pipe(gulp.dest('../public'));
});

gulp.task('concat:minify:vendor:js', function () {
    return gulp.src(config.javascripts.vendor)
        .pipe(concat('vendor.ext.js', {newLine: ';'}))
        .pipe(uglify())
        .pipe(gulp.dest('../public'));
});

gulp.task('concat:vendor:css', function () {
    return gulp.src(config.stylesheets.vendor)
        .pipe(modifyCssUrls({
            modify: function (url, filePath) {
                var parts = url.split('/');
                return 'fonts/' + parts[parts.length - 1];
            },
            prepend: '',
            append: ''
        }))
        .pipe(concatCss("vendor.css", {rebaseUrls: true}))
        .pipe(gulp.dest('../public'));
});

gulp.task('concat:minify:vendor:css', function () {
    return gulp.src(config.stylesheets.vendor)
        .pipe(modifyCssUrls({
            modify: function (url, filePath) {
                return 'fonts/' + url;
            },
            prepend: '',
            append: ''
        }))
        .pipe(concatCss("vendor.css", {rebaseUrls: false}))
        .pipe(minifyCSS({keepBreaks: true}))
        .pipe(gulp.dest('../public'));
});

gulp.task('concat:custom:css', function () {
    return gulp.src(config.stylesheets.custom)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('custom.css'))
        .pipe(gulp.dest('../public'));
});

gulp.task('concat:minify:custom:css', function () {
    return gulp.src(config.stylesheets.custom)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('custom.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('../public'));
});

gulp.task('i18n:copy', function () {
    return gulp.src('./src/assets/i18n/en.json')
        .pipe(gulp.dest('../public'));
});

gulp.task('fonts:copy', function () {
    return gulp.src('./src/assets/fonts/*.*')
        .pipe(gulp.dest('../public/fonts'));
});

gulp.task('watch:dev', function () {
    console.info('STARTING WATCH...');
    gulp.watch([
        './src/assets/stylesheets/scss/**/*.scss'
    ], ['concat:custom:css']);
    gulp.watch([
        './src/assets/i18n/en.json'
    ], ['i18n:copy']);
});

gulp.task('default', [
    'i18n:copy',
    'fonts:copy',
    'concat:vendor:js',
    'concat:vendor:css',
    'concat:custom:css',
    'webpack:app:dev'
]);

gulp.task('prod', [
    'i18n:copy',
    'fonts:copy',
    'concat:minify:vendor:js',
    'concat:minify:vendor:css',
    'concat:minify:custom:css',
    'webpack:app:prod'
]);

gulp.task('watch', [
    'i18n:copy',
    'fonts:copy',
    'concat:vendor:js',
    'concat:vendor:css',
    'concat:custom:css',
    'webpack:app:dev:watch',
    'watch:dev'
]);