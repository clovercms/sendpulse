import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppModule } from './app/';

declare var ENVIRONMENT: any;

if (ENVIRONMENT === 'development') {
    console.info('Running in development mode.');
} else {
    enableProdMode();
    console.info('Running in production mode.');
}

platformBrowserDynamic().bootstrapModule(AppModule);
