import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

import 'rxjs';

import 'ng2-translate';
import 'ng2-bootstrap/ng2-bootstrap';
// import 'angular2-jwt';
import 'moment'
import 'ng2-file-upload/ng2-file-upload';
