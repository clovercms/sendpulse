import { Routes } from '@angular/router';

import { LoginRoutes } from './auth/login/login.routes';
import { SignUpRoutes } from './auth/sign-up/sign-up.routes';
import { UserRoutes } from './user/user.routes';

export const routes: Routes = [
    ...LoginRoutes,
    ...SignUpRoutes,
    ...UserRoutes
];
