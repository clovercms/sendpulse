import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { UserComponent } from './user.component';

import { EventsModule } from './events';

@NgModule({
    imports: [
        RouterModule,
        SharedModule,
        EventsModule
    ],
    declarations: [UserComponent],
    exports: [UserComponent]
})

export class UserModule {
}
