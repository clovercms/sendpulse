import { Route } from '@angular/router';
import { UserComponent } from './user.component';
import { EventsComponent } from './events';
import { AuthUser } from "../auth.user";

export const UserRoutes: Route[] = [
    {
        path: 'panel',
        component: UserComponent,
        children: [
            {
                path: 'events',
                component: EventsComponent,
                canActivate: [AuthUser]
            }
        ]
    }
];