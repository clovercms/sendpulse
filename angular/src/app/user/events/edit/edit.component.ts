import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ng2-bootstrap/ng2-bootstrap';
import { EventService } from '../../../shared/services/event.service';
import * as moment from 'moment';

@Component({
    selector: 'edit-event',
    templateUrl: './edit.component.html',
    providers: [EventService]
})

export class EditComponent {

    /**
     *
     * @type {ModalDirective}
     */
    @ViewChild('modal') public modal: ModalDirective;

    /**
     *
     * @type {Array<any>}
     */
    public alerts: Array<any> = [];

    /**
     *
     * @type {boolean}
     */
    public submit: boolean = false;

    /**
     *
     * @type {boolean}
     */
    public disabled: boolean = false;

    /**
     *
     * @type {any}
     */
    public data: any;

    /**
     *
     */
    public handler: Function;

    /**
     *
     * @param {EventService} eventService
     */
    public constructor(private eventService: EventService) {
        this.initialize();
    }

    /**
     *
     * @param data {any}
     */
    public show(data: any = null) {
        this.initialize(data);
        this.submit = false;
        this.disabled = false;
        this.modal.show();
    }

    /**
     *
     * @param {Function} handler
     */
    public setSuccessHandler(handler: Function) {
        this.handler = handler;
    }

    /**
     *
     * @param data {any}
     */
    private initialize(data: any = null) {
        if (!data) {
            this.data = {title: '', description: '', date: '', time: ''};
        } else {
            const date = moment(data.date);
            this.data = {
                id: data.id,
                title: data.title,
                description: data.description,
                date: date.format('YYYY-MM-DD'),
                time: date.format('HH:mm')
            };
        }
    }

    /**
     *
     * @param {Event} event
     * @param {NgForm} form
     */
    public actionSave(event: Event, form: NgForm) {
        event.preventDefault();
        this.submit = true;
        if (form.valid) {
            this.disabled = true;
            const data: any = {
                date: `${this.data.date} ${this.data.time}`,
                title: this.data.title,
                description: this.data.description
            };
            if (this.data.id) {
                this.eventService.update(this.data.id, data).then(this.successHandler).catch(this.failedHandler);
            } else {
                this.eventService.create(data).then(this.successHandler).catch(this.failedHandler);
            }
        }
    }

    /**
     *
     */
    protected successHandler = () => {
        this.disabled = false;
        this.modal.hide();
        if (this.handler) {
            this.handler();
        }
    };

    /**
     *
     * @param response {any}
     */
    protected failedHandler = (response: any) => {
        const messages: Array<string> = JSON.parse(response._body).messages;
        this.alerts = messages.map(msg => {
            return {type: 'danger', msg: msg, timeout: 4000};
        });
        this.disabled = false;
    };
}
