import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule, AlertModule, TimepickerModule } from 'ng2-bootstrap/ng2-bootstrap';

import { EditComponent } from './edit.component';
import { SharedModule } from '../../../shared/shared.module';
import { BootstrapModule } from '../../../shared/extension/bootstrap';
import { ValidationModule } from '../../../shared/extension/validation';

@NgModule({
    imports: [
        FormsModule,
        ModalModule,
        AlertModule,
        SharedModule,
        BootstrapModule,
        ValidationModule,
        TimepickerModule
    ],
    declarations: [EditComponent],
    exports: [EditComponent]
})

export class EditModule {
}
