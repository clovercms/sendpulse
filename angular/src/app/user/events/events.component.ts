import { Component, OnInit, ViewChild } from '@angular/core';
import { EditComponent } from './edit';
import { RequestDataOptions, IRequestDataOptions, IField } from '../../shared/extension/dt-table/dt-table.service';
import { EventService } from "../../shared/services/event.service";
import { MdConfirmComponent } from "../../shared/extension/bootstrap/md-confirm/md-confirm.component";

@Component({
    selector: 'events',
    templateUrl: './events.component.html',
    providers: [EventService]
})

export class EventsComponent implements OnInit {

    /**
     *
     * @type {EditComponent}
     */
    @ViewChild('edit') public edit: EditComponent;

    /**
     *
     * @type {MdConfirmComponent}
     */
    @ViewChild('confirm') public confirm: MdConfirmComponent;

    /**
     *
     * @type {boolean}
     */
    public init: boolean = false;

    /**
     *
     * @type {Array<any>}
     */
    public list: Array<any> = [];

    /**
     *
     * @type {Array<IField>}
     */
    public fields: Array<IField> = [
        {title: 'date', name: 'date', search: false, width: 150},
        {title: 'title', name: 'title', search: true, width: 300},
        {title: 'description', name: 'description', search: true},
        {title: 'status', name: 'status', search: false, width: 80},
        {title: 'action', cls: 'text-right', width: 220},
    ];

    /**
     *
     * @type {IRequestDataOptions}
     */
    public options: IRequestDataOptions = new RequestDataOptions({
        offset: 0, limit: 10, searchFields: [
            'title',
            'description'
        ]
    });

    /**
     *
     * @param {EventService} eventService
     */
    public constructor(private eventService: EventService) {

    }

    /**
     *
     */
    public ngOnInit() {
        this.getList(this.options);
        this.edit.setSuccessHandler(() => this.getList(this.options));
    }

    /**
     *
     * @param {IRequestDataOptions} options
     * @param {any} callback
     */
    public getList(options: IRequestDataOptions, callback: any = null) {
        this.options.disabled = true;
        this.eventService.list(options)
            .then((response: any) => {
                this.options.disabled = false;
                this.options.count = response.data.count;
                this.list = response.data.list;
                this.init = true;
                if (typeof callback == 'function') callback();
            })
            .catch(() => null);
    }

    /**
     *
     */
    public actionCreate = () => this.edit.show();


    /**
     *
     * @param item {any}
     */
    public actionComplete(item: any) {
        this.confirm.show({
            message: 'Complete event?',
            title: 'Confirm',
            success: () => this.eventService.complete(item.id)
                .then(() => this.getList(this.options, () => this.confirm.hide()))
        });
    }

    /**
     *
     * @param item {any}
     */
    public actionEdit = (item: any) => this.edit.show(item);

    /**
     *
     * @param item {any}
     */
    public actionDelete(item: any) {
        this.confirm.show({
            message: 'Delete event?',
            title: 'Confirm',
            success: () => this.eventService.destroy(item.id)
                .then(() => this.getList(this.options, () => this.confirm.hide()))
        });
    }
}
