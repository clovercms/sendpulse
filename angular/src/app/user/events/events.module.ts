import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { EventsComponent } from './events.component';
import { HelpersModule } from '../../shared/extension/helpers';
import { EditModule } from './edit';
import { DtTableModule } from '../../shared/extension/dt-table';
import { BootstrapModule } from '../../shared/extension/bootstrap';

@NgModule({
    imports: [
        SharedModule,
        HelpersModule,
        EditModule,
        DtTableModule,
        BootstrapModule
    ],
    declarations: [EventsComponent],
    exports: [EventsComponent]
})

export class EventsModule {
}
