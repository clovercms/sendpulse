import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../shared/services/user.service';

@Component({
    selector: 'user',
    templateUrl: './user.component.html',
    providers: [UserService]
})

export class UserComponent {

    /**
     *
     * @param {Router} router
     * @param {UserService} userService
     */
    public constructor(private router: Router, private userService: UserService) {

    }

    /**
     *
     */
    public actionLogout = () => {
        localStorage.clear();
        this.userService.logout().then(() => this.router.navigate(['/']))
    }
}
