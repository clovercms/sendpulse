import { Component, ViewContainerRef } from '@angular/core';
import { TranslateService } from 'ng2-translate';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    /**
     * @type {any}
     */
    static section: any;

    /**
     * @type {ViewContainerRef}
     */
    private viewContainerRef: ViewContainerRef;

    /**
     *
     * @param {TranslateService} translate
     * @param {ViewContainerRef} viewContainerRef
     */
    public constructor(translate: TranslateService, viewContainerRef: ViewContainerRef) {
        translate.setDefaultLang('en');
        translate.use('en');
        this.viewContainerRef = viewContainerRef;
    }
}