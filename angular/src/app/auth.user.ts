import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthUser implements CanActivate {

    /**
     *
     * @param {Router} router
     */
    public constructor(private router: Router) {

    }

    /**
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @return {boolean}
     */
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!localStorage.getItem('auth')) {
            this.router.navigate(['/']);
            return false;
        }
        return true;
    }
}