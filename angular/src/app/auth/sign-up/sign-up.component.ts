import { Component } from '@angular/core';
import { NgForm } from "@angular/forms";

import { ICreate, UserService, IResponse } from '../../shared/services/user.service';

@Component({
    selector: 'auth-sign-up',
    templateUrl: './sign-up.component.html',
    providers: [UserService]
})

export class SignUpComponent {

    /**
     *
     * @type {boolean}
     */
    public submit: boolean = false;

    /**
     *
     * @type {boolean}
     */
    public disabled: boolean = false;

    /**
     *  @type {ICreate}
     */
    public data: ICreate;

    /**
     *
     * @type {Array<any>}
     */
    public alerts: Array<any> = [];

    /**
     *
     * @type {boolean}
     */
    public done: boolean = false;

    /**
     *
     * @param {UserService} userService
     */
    public constructor(private userService: UserService) {
        this.data = {email: '', password: '', confirmPassword: ''};
    }

    /**
     *
     * @param {Event} event
     * @param {NgForm} form
     */
    public actionSignUp(event: Event, form: NgForm) {
        event.preventDefault();
        this.submit = true;
        if (form.valid) {
            this.disabled = true;
            this.userService.create(this.data).then((response: IResponse) => {
                this.disabled = false;
                this.done = true;
            }).catch((error: any) => {
                const messages: Array<string> = JSON.parse(error._body).messages;
                this.alerts = messages.map(item => {
                    return {type: 'danger', msg: item, timeout: 4000}
                });
                this.disabled = false;
            });
        }
    }
}
