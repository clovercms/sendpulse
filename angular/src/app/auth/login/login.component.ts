import { Component } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router } from '@angular/router';

import { ILogin, UserService, IResponse } from '../../shared/services/user.service';

@Component({
    selector: 'auth-login',
    templateUrl: './login.component.html',
    providers: [UserService]
})

export class LoginComponent {

    /**
     *
     * @type {boolean}
     */
    public submit: boolean = false;

    /**
     *
     * @type {boolean}
     */
    public disabled: boolean = false;

    /**
     *  @type {ILogin}
     */
    public data: ILogin;

    /**
     *
     * @type {Array<any>}
     */
    public alerts: Array<any> = [];

    /**
     *
     * @param {UserService} userService
     * @param {Router} router
     */
    public constructor(private userService: UserService,
                       private router: Router) {
        this.data = {email: '', password: ''};
    }

    /**
     *
     * @param {Event} event
     * @param {NgForm} form
     */
    public actionLogin(event: Event, form: NgForm) {
        event.preventDefault();
        this.submit = true;
        if (form.valid) {
            this.disabled = true;
            this.userService.login(this.data).then((response: IResponse) => {
                localStorage.setItem('auth', '1');
                this.router.navigate(['/panel/events']);
                this.disabled = false;
            }).catch((error: any) => {
                const messages: Array<string> = JSON.parse(error._body).messages;
                this.alerts = messages.map(msg => {
                    return {type: 'danger', msg: msg, timeout: 4000};
                });
                this.disabled = false;
            });
        }
    }
}
