import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AlertModule } from 'ng2-bootstrap/ng2-bootstrap';

import { SharedModule } from '../../shared/shared.module';
import { LoginComponent } from './login.component';
import { ValidationModule } from '../../shared/extension/validation';
import { BootstrapModule } from '../../shared/extension/bootstrap';

@NgModule({
    imports: [
        RouterModule,
        SharedModule,
        AlertModule,
        ValidationModule,
        FormsModule,
        BootstrapModule
    ],
    declarations: [LoginComponent],
    exports: [LoginComponent]
})
export class LoginModule {
}
