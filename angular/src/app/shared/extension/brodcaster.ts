import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

interface BroadcastEvent {
    key: any;
    data?: any;
}

export class Broadcaster {

    /**
     *
     * @type {Subject<BroadcastEvent>}
     */
    private eventBus: Subject<BroadcastEvent>;

    /**
     *
     */
    public constructor() {
        this.eventBus = new Subject<BroadcastEvent>();
    }

    /**
     *
     * @param {any} key
     * @param {data} data
     */
    public broadcast(key: any, data?: any) {
        this.eventBus.next({key, data});
    }

    /**
     *
     * @param {any} key
     * @return {Observable<R>}
     */
    public on<T>(key: any): Observable<T> {
        return this.eventBus.asObservable()
            .filter(event => event.key === key)
            .map(event => <T>event.data);
    }
}
