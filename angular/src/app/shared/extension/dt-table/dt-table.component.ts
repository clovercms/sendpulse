import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IField, RequestDataOptions, IRequestDataOptions } from './dt-table.service';

@Component({
    selector: 'dt-table',
    templateUrl: 'dt-table.component.html'
})

export class DtTableComponent {

    /**
     *
     * @type {EventEmitter}
     */
    @Output()
    public refresh = new EventEmitter();

    /**
     *
     * @type {IRequestDataOptions}
     */
    @Input()
    public options: IRequestDataOptions = new RequestDataOptions();

    /**
     *
     * @type {Array<number>}
     */
    @Input()
    public ranges: Array<number> = [5, 10, 25, 50, 100];

    /**
     *
     * @type {number}
     */
    @Input()
    public displayPages: number = 5;

    /**
     *
     * @type {number}
     */
    @Input()
    public displayCount:number = 10;

    /**
     *
     * @type {Array<IField>}
     */
    @Input()
    public fields: Array<IField> = [];


    /**
     *
     * @param {IRequestDataOptions} options
     */
    public actionRefresh(options: IRequestDataOptions) {
        // this.options = JSON.parse(JSON.stringify(options));
        this.options = options;
        this.refresh.emit(this.options);
    }

}
