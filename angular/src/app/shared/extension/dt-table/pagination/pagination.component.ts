import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Pagination } from './pagination';

import { RequestDataOptions, IRequestDataOptions } from '../dt-table.service';

@Component({
    selector: 'pagination',
    styles: ['pagination{display: block;}'],
    templateUrl: 'pagination.component.html'
})

export class PaginationComponent implements OnChanges {

    /**
     *
     * @type {RequestDataOptions}
     */
    @Input()
    public options:IRequestDataOptions = new RequestDataOptions();

    /**
     *
     * @type {boolean}
     */
    @Input()
    public displayRange: boolean = true;

    /**
     *
     * @type {number}
     */
    @Input()
    public displayPages: number = 5;

    /**
     *
     * @type {EventEmitter}
     */
    @Output()
    public refresh = new EventEmitter();

    /**
     *
     * @type {Pagination}
     */
    protected _pagination: Pagination;

    /**
     *
     */
    public constructor() {
        this._pagination = new Pagination();
    }

    /**
     *
     * @param changes
     */
    public ngOnChanges(changes: any) {
        this._pagination.setFoundRows(this.options.count);
        this._pagination.setCurrentPage(this.options.page);
        this._pagination.setLimit(this.options.limit);
        this._pagination.setDisplayRange(this.displayRange);
        this._pagination.setDisplayPages(this.displayPages);
    }

    /**
     *
     * @return {number[]}
     */
    private getPages(): Array<number> {
        return this._pagination.getPages();
    }

    /**
     *
     * @returns {boolean}
     */
    private isNotFirstPage() {
        return this._pagination.getCurrentPage() != 1;
    }

    /**
     *
     * @returns {boolean}
     */
    private isNotLastPage() {
        return this._pagination.getCurrentPage() != this._pagination.getLastPage();
    }

    /**
     *
     * @param {number} page
     */
    private toPage(page: number) {
        if(this.options.disabled) return;
        this._pagination.setCurrentPage(page);
        this.options.page = this._pagination.getCurrentPage();
        this.options.limit = this._pagination.getLimit();
        this.options.offset = this._pagination.getOffset();
        this.refresh.emit(this.options);
    }

    /**
     *
     */
    private toPreviousPage() {
        if(this.options.disabled) return;
        if (this.isNotFirstPage()) this.toPage(this._pagination.getCurrentPage() - 1);
    }

    /**
     *
     */
    private toNextPage() {
        if(this.options.disabled) return;
        if (this.isNotLastPage()) this.toPage(this._pagination.getCurrentPage() + 1);
    }

    /**
     *
     * @param {number} page
     * @returns {boolean}
     */
    private isCurrentPage(page: number) {
        return page == this._pagination.getCurrentPage();
    }

    /**
     *
     * @returns {number}
     */
    private getLastPage(): number {
        return this._pagination.getLastPage();
    }

    /**
     *
     * @param {number} page
     * @returns {boolean}
     */
    private isRange(page: number): boolean {
        return page == 0;
    }
}
