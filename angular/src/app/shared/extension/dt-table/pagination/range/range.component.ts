import { Component, Input, Output, EventEmitter } from '@angular/core';

import { RequestDataOptions, IRequestDataOptions } from '../../dt-table.service';

@Component({
    selector: 'range',
    templateUrl: 'range.component.html',
    styleUrls: ['range.component.css']
})

export class RangeComponent {

    /**
     *
     * @type {IRequestDataOptions}
     */
    @Input()
    public options:IRequestDataOptions = new RequestDataOptions();

    /**
     *
     * @type {Array<number>}
     */
    @Input()
    public ranges: Array<number> = [10, 25, 50, 100];

    /**
     *
     * @type {EventEmitter}
     */
    @Output()
    public refresh = new EventEmitter();

    /**
     *
     */
    public actionSetRange(event: any) {
        this.options.limit = parseInt(event.target.value);
        this.options.offset = 0;
        this.options.page = 1;
        this.refresh.emit(this.options);
    }
}
