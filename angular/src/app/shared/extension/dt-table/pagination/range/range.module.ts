import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule}   from '@angular/forms';

import {RangeComponent} from './range.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [RangeComponent],
    exports: [RangeComponent]
})

export class RangeModule {
}

