import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared.module';
import { SortingComponent } from './sorting.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [SortingComponent],
    exports: [SortingComponent]
})

export class SortingModule {
}

