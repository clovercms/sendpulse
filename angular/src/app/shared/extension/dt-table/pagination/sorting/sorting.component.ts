import { Component, Input, Output, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';

import { IRequestDataOptions } from '../../dt-table.service';

@Component({
    selector: '[sorting]',
    templateUrl: 'sorting.component.html',
    styles: ['span{cursor: pointer; display: inline-block; width: 100%;}']
})

export class SortingComponent implements AfterViewInit {

    /**
     *
     * @type {string}
     */
    @Input()
    public title: string;

    /**
     *
     * @type {string}
     */
    @Input()
    public name: string;

    /**
     *
     * @type {string}
     */
    @Input()
    public cls: string;

    /**
     *
     * @type {number}
     */
    @Input()
    public width: number;

    /**
     *
     * @type {IRequestDataOptions}
     */
    @Input()
    public options: IRequestDataOptions;


    /**
     *
     * @type {EventEmitter}
     */
    @Output()
    public refresh = new EventEmitter();

    /**
     *
     * @param {ElementRef} element
     */
    public constructor(private element: ElementRef) {

    }

    public ngAfterViewInit() {
        if (this.cls) {
            this.element.nativeElement.classList.add(this.cls);
        }
        if (this.width) {
            this.element.nativeElement.style.width = `${this.width}px`;
        }
    }

    /**
     *
     */
    public actionSorting() {
        if (this.options.disabled) return;
        if (this.options.sortField === this.name && this.options.sortDirection === 'desc') {
            delete this.options.sortField;
            delete this.options.sortDirection;
        } else if (this.options.sortField === this.name && this.options.sortDirection === 'asc') {
            this.options.sortDirection = 'desc';
        } else {
            this.options.sortDirection = 'asc';
            this.options.sortField = this.name;
        }
        this.refresh.emit(this.options);
    }

    /**
     *
     * @return {boolean}
     */
    public isSortingAsc() {
        let asc: boolean = this.name == this.options.sortField && this.options.sortDirection == 'asc';
        if (asc) {
            this.element.nativeElement.style.backgroundColor = '#eee';
        }
        return asc;
    }

    /**
     *
     * @return {boolean}
     */
    public isSortingDesc() {
        let desc: boolean = this.name == this.options.sortField && this.options.sortDirection == 'desc';
        if (desc) {
            this.element.nativeElement.style.backgroundColor = '#eee';
        }
        return desc;
    }

    /**
     *
     * @return {boolean}
     */
    public isNotSorting() {
        let not: boolean = this.name != this.options.sortField;
        if (not) {
            this.element.nativeElement.style.backgroundColor = 'inherit';
        }
        return not;
    }
}
