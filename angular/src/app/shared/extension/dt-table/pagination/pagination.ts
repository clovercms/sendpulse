export class Pagination {

    /**
     *
     * @type {number}
     * @private
     */
    private limit: number = 10;

    /**
     *
     * @type {number}
     * @private
     */
    private offset: number = 0;

    /**
     *
     * @type {number}
     * @private
     */
    private foundRows: number = 0;

    /**
     *
     * @type {number}
     * @private
     */
    private currentPage: number = 1;

    /**
     *
     * @type {number}
     * @private
     */
    private displayPages: number = 5;

    /**
     *
     * @type {boolean}
     * @private
     */
    private displayRange: boolean = true;

    /**
     *
     * @param {number} limit
     * @returns {Pagination}
     */
    public setLimit(limit: number) {

        if (limit % 1 === 0 && limit > 0) {

            this.limit = limit;
            this.offset = (this.currentPage - 1) * this.limit;
        } else {

            throw new Error('limit must be an integer greater than zero.');
        }

        return this;
    }

    /**
     *
     * @param {number} offset
     * @returns {Pagination}
     */
    public setOffset(offset: number) {

        if (offset % 1 === 0 && offset >= 0) {

            this.offset = offset;
            this.currentPage = this.offset ? Math.ceil((this.offset + 1) / this.limit) : 1;
        } else {

            throw new Error('offset must be an integer greater.');
        }

        return this;
    }

    /**
     *
     * @param {number} currentPage
     * @returns {Pagination}
     */
    public setCurrentPage(currentPage: number) {
        if (currentPage % 1 == 0 && currentPage >= 1) {

            this.currentPage = currentPage;
            this.offset = (this.currentPage - 1) * this.limit;
        } else {

            throw new Error('page must be an integer greater than zero.');
        }

        return this;
    }

    /**
     *
     * @param {number} foundRows
     * @returns {Pagination}
     */
    public setFoundRows(foundRows: number) {

        if (foundRows % 1 === 0 && foundRows >= 0) {

            this.foundRows = foundRows;

            if (this.foundRows === 0) {

                this.setCurrentPage(1);
            } else if (this.foundRows < this.currentPage * this.limit) {

                this.setCurrentPage(Math.ceil(this.foundRows / this.limit));
            }
        } else {

            throw new Error('foundRows must be an integer greater.');
        }
        return this;
    }

    /**
     *
     * @param {number} display
     * @returns {Pagination}
     */
    public setDisplayPages(display: number) {

        if (display < 3 || display % 1 !== 0) {

            display = 3;
        } else if (display % 2 == 0) {

            ++display;
        }

        this.displayPages = display;

        return this;
    }

    /**
     *
     * @param {boolean} display
     * @returns {Pagination}
     */
    public setDisplayRange(display: boolean) {

        this.displayRange = display;

        return this;
    }

    /**
     *
     * @returns {number[]}
     */
    public getPages(): number[] {

        let countPages: number = Math.ceil(this.foundRows / this.limit);
        let pages: number[] = [];

        if (countPages - this.displayPages <= (this.displayRange ? 1 : 0)) {

            for (let i: number = 1; i <= countPages; ++i) {

                pages.push(i);
            }
        } else if (this.currentPage <= this.displayPages - 1) {

            for (let i: number = 1; i <= this.displayPages; ++i) {

                pages.push(i);
            }
            pages = this.addTargetRange(pages, countPages);

        } else if (this.currentPage >= countPages - this.displayPages + 2) {

            pages = this.addInitialRange(pages);

            for (let i: number = countPages - this.displayPages + 1; i <= countPages; ++i) {

                pages.push(i);
            }
        } else {

            let start: number = this.currentPage - Math.floor(this.displayPages / 2);
            let stop: number = this.currentPage + Math.floor(this.displayPages / 2);
            pages = this.addInitialRange(pages);

            for (let i: number = start; i <= stop; ++i) {

                pages.push(i);
            }

            pages = this.addTargetRange(pages, countPages);
        }
        return pages;
    };

    /**
     *
     * @param {number[]} pages
     * @returns {number[]}
     */
    private addInitialRange(pages: number[]): number[] {

        if (this.displayRange) {

            pages.push(1);
            pages.push(0);
        }

        return pages;
    }

    /**
     *
     * @param {number[]} pages
     * @param {number} countPages
     * @returns {number[]}
     */
    private addTargetRange(pages: number[], countPages: number): number[] {

        if (this.displayRange) {

            pages.push(0);
            pages.push(countPages);
        }

        return pages;
    }

    /**
     *
     * @returns {number}
     */
    public getLimit(): number {

        return this.limit;
    }

    /**
     *
     * @returns {number}
     */
    public getOffset(): number {

        return this.offset;
    }

    /**
     *
     * @returns {number}
     */
    public getCurrentPage(): number {

        return this.currentPage;
    }

    /**
     *
     * @returns {number}
     */
    public getLastPage(): number {

        return Math.ceil(this.foundRows / this.limit);
    };

    /**
     *
     * @returns {number}
     */
    public getPreviousPage(): number {

        return this.currentPage > 1 ? this.currentPage - 1 : 0;
    }

    /**
     *
     * @returns {number}
     */
    public getNextPage() {

        return this.currentPage < Math.ceil(this.foundRows / this.limit) ? this.currentPage + 1 : false;
    }
}
