import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DtTableComponent } from './dt-table.component';

import { PaginationModule } from './pagination';
import { SortingModule } from './pagination/sorting';
import { RangeModule } from './pagination/range';
import { SearchModule } from './search';

@NgModule({
    imports: [
        CommonModule,
        PaginationModule,
        SortingModule,
        RangeModule,
        SearchModule,
    ],
    declarations: [DtTableComponent],
    exports: [DtTableComponent, SearchModule]
})

export class DtTableModule {
}
