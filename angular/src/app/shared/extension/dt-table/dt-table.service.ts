import { URLSearchParams } from '@angular/http';

export interface IField {
    title?: string;
    name?: string;
    search?: boolean;
    cls?: string;
    width?: number;
}

export interface IRequestDataOptions {
    offset: number;
    limit: number;
    sortField?: string;
    sortDirection?: string;
    searchFields?: Array<string>;
    searchText?: string;
    filters?: Array<{ name: string, value: string }>;
    count?: number;
    page?: number;
    disabled?: boolean;
    getQueryString?: any
}

export interface IResponseDataList {
    status: string;
    message: string;
    data: {
        list: Array<any>;
        count: number;
    }
}

export class RequestDataOptions implements IRequestDataOptions {

    /**
     *
     * @type {number}
     */
    public offset: number;

    /**
     *
     * @type {number}
     */
    public limit: number;

    /**
     *
     * @type {string}
     */
    public sortField: string;

    /**
     *
     * @type {string}
     */
    public sortDirection: string;

    /**
     *
     * @type {Array<string>}
     */
    public searchFields: Array<string>;

    /**
     *
     * @type {string}
     */
    public searchText: string;

    /**
     *
     * @type {object}
     */
    public filters: Array<{ name: string, value: string }>;

    /**
     *
     * @type {number}
     */
    public count: number = 0;

    /**
     *
     * @type {number}
     */
    public page: number = 1;

    /**
     *
     * @type {Array<any>}
     */
    protected queryField: Array<any> = ['page', 'sortField', 'sortDirection', 'searchText', 'limit', 'offset'];

    /**
     *
     * @param {IRequestDataOptions} options
     */
    public constructor(options: IRequestDataOptions = {offset: 0, limit: 10}) {
        this.offset = options.offset;
        this.limit = options.limit;
        if (options.sortField) this.sortField = options.sortField;
        if (options.sortDirection) this.sortDirection = options.sortDirection;
        if (options.searchFields) this.searchFields = options.searchFields;
        if (options.searchText) this.searchText = options.searchText;
        if (options.filters) this.filters = options.filters;
        if (options.count) this.count = options.count;
        if (options.page) this.page = options.page;
    }

    /**
     *
     * @return {string}
     */
    public toString(): string {
        return this.getQueryString().toString();
    }

    /**
     *
     * @return {URLSearchParams}
     */
    public getQueryString(): any {

        let params = new URLSearchParams();

        this.queryField.forEach((key: string) => {
            let self: any = this;
            if (this.hasOwnProperty(key) && self[key]) {
                params.set(key, self[key]);
            }
        });

        if (this.searchFields) {
            this.searchFields.forEach((item: string, index: number) => {
                params.append(`searchFields[${index}]`, item)
            });
        }

        if (this.filters) {
            this.filters.forEach((item: any, index: number) => {
                params.append(`filters[${index}][name]`, item.name);
                params.append(`filters[${index}][value]`, item.value);
            });
        }

        return params;
    }

}