import { Component, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';

import { RequestDataOptions, IRequestDataOptions } from '../dt-table.service';

@Component({
    selector: 'search',
    templateUrl: 'search.component.html'
})

export class SearchComponent {

    /**
     *
     * @type {ElementRef}
     */
    @ViewChild('focus') public focus: ElementRef;

    /**
     *
     * @type {IRequestDataOptions}
     */
    @Input('options')
    set opt(options: IRequestDataOptions) {
        this.options = options;
        if (this.enterStatus && this.focus) {
            this.enterStatus = false;
            setTimeout(() => this.focus.nativeElement.focus(), 600);
        }
    }

    /**
     *
     * @type {IRequestDataOptions}
     */
    public options: IRequestDataOptions = new RequestDataOptions();

    /**
     *
     * @type {EventEmitter}
     */
    @Output()
    public refresh = new EventEmitter();

    /**
     *
     * @type {boolean}
     */
    public enterStatus: boolean = false;

    /**
     *
     * @param {KeyboardEvent} event
     */
    public enter(event: KeyboardEvent) {
        if (event.keyCode === 13) {
            this.enterStatus = true;
            this.actionSearch();
        }
    }

    /**
     *
     */
    public actionSearch() {
        this.options.offset = 0;
        this.options.page = 1;
        this.options.searchText = this.options.searchText ? this.options.searchText.trim() : '';
        this.refresh.emit(this.options);
    }
}
