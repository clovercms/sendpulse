import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TimepickerModule } from 'ng2-bootstrap/ng2-bootstrap';

import { MdTimepickerComponent } from './md-timepicker.component';

@NgModule({
    imports: [
        FormsModule,
        TimepickerModule
    ],
    declarations: [MdTimepickerComponent],
    exports: [MdTimepickerComponent]
})

export class MdTimepickerModule {

}

