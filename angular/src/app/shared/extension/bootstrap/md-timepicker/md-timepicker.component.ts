import { Component, Input, Output, EventEmitter, ElementRef, ViewChild, OnInit } from '@angular/core';

import * as moment from 'moment';

@Component({
    selector: 'md-timepicker',
    templateUrl: 'md-timepicker.component.html',
    styleUrls: ['md-timepicker.component.css']
})

export class MdTimepickerComponent implements OnInit {

    /**
     *
     * @type {ElementRef}
     */
    @ViewChild('timepickerx') public timepickerx: ElementRef;

    /**
     *
     * @type {string}
     */
    public format: string = 'HH:mm';

    /**
     *
     * @type {string}
     */
    @Input('date')
    public set inputDate(date: string) {
        if (moment(date, this.format, true).isValid()) {
            this._date = moment(date, this.format).toDate();
        }
    }

    /**
     *
     * @type {EventEmitter}
     */
    @Output('dateChange')
    public outputDate = new EventEmitter(false);

    /**
     *
     * @param {any} date
     */
    public set date(date: any) {
        this._date = date;
        this.outputDate.emit(moment(this._date).format(this.format));
    }

    /**
     *
     * @return {any}
     */
    public get date(): any {
        return this._date;
    }

    /**
     *
     * @type {any}
     */
    private _date: any;

    /**
     *
     */
    public ngOnInit() {
        this.timepickerx.nativeElement.addEventListener('click', (event: Event) => {
            event.preventDefault();
            event.stopPropagation();
        });
        document.getElementsByTagName('body')[0]
            .addEventListener('click', (event: Event) => this.hide());
    }

    /**
     *
     * @param {Event | null = null} event
     */
    public show(event: Event | null = null) {
        if (event) {
            event.stopPropagation();
        }
        setTimeout(() => this.timepickerx.nativeElement.classList.remove('hide'), 200);
    }

    /**
     *
     * @param {Event | null = null} event
     */
    public hide(event: Event | null = null) {
        if (event) {
            event.stopPropagation();
        }
        this.timepickerx.nativeElement.classList.add('hide');
    }

    /**
     *
     * @param {Event | null = null} event
     */
    public toggle(event: Event | null = null) {
        if (event) {
            event.stopPropagation();
        }
        setTimeout(() => this.timepickerx.nativeElement.classList.toggle('hide'), 200);
    }

}