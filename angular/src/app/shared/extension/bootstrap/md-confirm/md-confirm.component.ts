import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap/ng2-bootstrap';

export interface IDialogOptions {
    title?: string;
    message: string;
    btnCancel?: string|false;
    btnOk?: string|false;
    success?: any,
    class?: string,
}

@Component({
    selector: 'md-confirm',
    templateUrl: 'md-confirm.component.html',
})

export class MdConfirmComponent {

    /**
     *
     * @type {ModalDirective}
     */
    @ViewChild('confirmDialog') public confirmDialog: ModalDirective;

    /**
     *
     * @type {IDialogOptions}
     * @private
     */
    private options: IDialogOptions = {message: ''};

    /**
     *
     * @type {boolean}
     * @private
     */
    private disabled: boolean = false;

    /**
     *
     * @type {Array<any>}
     * @private
     */
    private alerts: Array<any> = []; // @todo

    /**
     *
     * @param {IDialogOptions} options
     */
    public show(options: IDialogOptions = {message: ''}) {
        this.message(options);
        this.confirmDialog.show();
    };

    /**
     *
     */
    public hide = () => this.confirmDialog.hide();

    /**
     *
     * @param {IDialogOptions} options
     */
    public message(options: IDialogOptions) {
        this.options = options;
        this.disabled = false;
    }

    /**
     *
     * @private
     */
    private actionCancel = () => this.confirmDialog.hide();

    /**
     *
     * @private
     */
    private actionConfirm() {
        this.disabled = true;
        if (typeof this.options.success == 'function') {
            this.options.success();
        }
    }

}