import { NgModule } from '@angular/core';
import { ModalModule } from 'ng2-bootstrap/ng2-bootstrap';
import { SharedModule } from '../../../shared.module';
import { MdConfirmComponent } from './md-confirm.component';

@NgModule({
    imports: [
        ModalModule,
        SharedModule
    ],
    declarations: [MdConfirmComponent],
    exports: [MdConfirmComponent]
})

export class MdConfirmModule {

}