import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DatepickerModule } from 'ng2-bootstrap/ng2-bootstrap';

import { MdDatepickerComponent } from './md-datepicker.component';

@NgModule({
    imports: [
        FormsModule,
        DatepickerModule
    ],
    declarations: [MdDatepickerComponent],
    exports: [MdDatepickerComponent]
})

export class MdDatepickerModule {

}

