import { Component, Input, Output, EventEmitter, ElementRef, ViewChild, OnInit } from '@angular/core';

import * as moment from 'moment';

@Component({
    selector: 'md-datepicker',
    templateUrl: 'md-datepicker.component.html',
    styleUrls: ['md-datepicker.component.css']
})

export class MdDatepickerComponent implements OnInit {

    /**
     *
     * @type {ElementRef}
     */
    @ViewChild('datepickerx') public datepickerx: ElementRef;

    /**
     *
     * @type {string}
     */
    @Input() public format: string = 'YYYY-MM-DD';

    /**
     *
     * @type {string}
     */
    @Input('date')
    public set inputDate(date: string) {
        if (moment(date, this.format, true).isValid()) {
            this._date = moment(date, this.format).toDate();
        }
    }

    /**
     *
     * @type {EventEmitter}
     */
    @Output('dateChange')
    public outputDate = new EventEmitter(false);

    /**
     *
     * @param {any} date
     */
    public set date(date: any) {
        this._date = date;
        this.outputDate.emit(moment(this._date).format(this.format));
        this.hide();
    }

    /**
     *
     * @return {any}
     */
    public get date(): any {
        return this._date;
    }

    /**
     *
     * @type {any}
     */
    private _date: any;

    /**
     *
     */
    public ngOnInit() {
        this.datepickerx.nativeElement.addEventListener('click', (event: Event) => event.stopPropagation());
        document.getElementsByTagName('body')[0].addEventListener('click', (event: Event) => this.hide());
    }

    /**
     *
     * @param {Event | null = null} event
     */
    public show(event: Event | null = null) {
        if (event) {
            event.stopPropagation();
        }
        setTimeout(() => this.datepickerx.nativeElement.classList.remove('hide'), 200);
    }

    /**
     *
     * @param {Event | null = null} event
     */
    public hide(event: Event | null = null) {
        if (event) {
            event.stopPropagation();
        }
        this.datepickerx.nativeElement.classList.add('hide');
    }

    /**
     *
     * @param {Event | null = null} event
     */
    public toggle(event: Event | null = null) {
        if (event) {
            event.stopPropagation();
        }
        setTimeout(() => this.datepickerx.nativeElement.classList.toggle('hide'), 200);
    }

}