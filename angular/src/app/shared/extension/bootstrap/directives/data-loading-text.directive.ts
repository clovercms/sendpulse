import { Directive, Input, ElementRef } from '@angular/core';

@Directive({selector: '[dataLoadingText]'})
export class DATA_LOADING_TEXT {

    /**
     *
     * @type {string}
     */
    @Input('dataLoadingText')
    public set loadingText(str: string) {
        this.text = str;
    }

    /**
     *
     * @type {string}
     */
    public text: string;

    /**
     *
     * @type {boolean}
     */
    @Input('loading')
    public set status(status: boolean) {
        this.loading = status;
        if (!this.html && status) {
            this.html = this.elementRef.nativeElement.innerHTML;
        }
        if (this.html) {
            this.elementRef.nativeElement.innerHTML = status ?
                '<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>&nbsp;&nbsp;' + this.text :
                this.html;
        }
    }

    /**
     *
     * @type {boolean}
     */
    public loading: boolean = false;

    /**
     *
     * @type {string}
     */
    public html: string;

    /**
     *
     * @param {ElementRef} elementRef
     */
    public constructor(private elementRef: ElementRef) {
    }

}

