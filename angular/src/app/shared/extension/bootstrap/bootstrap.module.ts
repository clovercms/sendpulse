import { NgModule } from "@angular/core";

import { DATA_LOADING_TEXT } from "./directives/data-loading-text.directive";
import { MdDatepickerModule } from './md-datepicker';
import { MdTimepickerModule } from './md-timepicker';
import { MdConfirmModule } from './md-confirm';

const BOOTSTRAP_DIRECTIVES = [
    DATA_LOADING_TEXT
];

@NgModule({
    imports: [MdDatepickerModule, MdTimepickerModule, MdConfirmModule],
    declarations: [BOOTSTRAP_DIRECTIVES],
    exports: [
        BOOTSTRAP_DIRECTIVES,
        MdDatepickerModule,
        MdTimepickerModule,
        MdConfirmModule
    ],
})

export class BootstrapModule {
}
