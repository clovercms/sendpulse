import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'dateFormat'})
export class DateFormat implements PipeTransform {
    /**
     *
     * @param {string} value
     * @param {any} options
     * @return {string}
     */
    public transform(value: string, options: any = {format: 'YYYY-MM-DD HH:mm', from: 'YYYY-MM-DD HH:mm'}): string {
        moment.locale("en");
        return moment(value, options.from).format(options.format);
    }
}