import { NgModule } from '@angular/core';

import { DateFormat } from './date-format.pipe';

@NgModule({
    imports: [],
    declarations: [DateFormat],
    exports: [DateFormat]
})

export class HelpersModule {
}
