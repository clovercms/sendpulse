import { ValidatorFn, AbstractControl, Validators, FormGroup } from '@angular/forms';

import * as moment from 'moment';

function isPresent(obj: any): boolean {
    return obj !== undefined && obj !== null;
}

export class CustomValidators {

    /**
     * Validator that requires controls to have a value of email.
     */
    static email(control: AbstractControl): { [key: string]: boolean } {
        if (isPresent(Validators.required(control))) return null;
        const v: string = control.value;
        return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(v) ? null : {'email': true};
    }

    static date(control: AbstractControl): { [key: string]: boolean } {
        if (isPresent(Validators.required(control))) return null;
        return moment(control.value, "YYYY-MM-DD", true).isValid() ? null : {'date': true};
    }

    static time(control: AbstractControl): { [key: string]: boolean } {
        if (isPresent(Validators.required(control))) return null;
        return moment(control.value, "HH:mm", true).isValid() ? null : {'time': true};
    }

    /**
     * Validator that requires controls to have a value to equal another value.
     */
    static equal(val: any): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (isPresent(Validators.required(control))) return null;
            const v: any = control.value;
            return val === v ? null : {equal: true};
        };
    }

    /**
     * Validator that requires controls to have a value to equal another control.
     */
    static equalTo(group: FormGroup): { [key: string]: boolean } {
        const keys: string[] = Object.keys(group.controls);
        const len: number = keys.length;
        if (!len) return null;
        const firstKey = keys[0];
        for (let i = 1; i < len; i++) {
            if (group.controls[firstKey].value !== group.controls[keys[i]].value) {
                return {equalTo: true};
            }
        }

        return null;
    }

    static afterCurrentTime(group: FormGroup): { [key: string]: boolean } {
        const keys: any = Object.keys(group.controls);
        const len: number = keys.length;
        if (keys.length !== 2) return null;
        const time = moment(`${group.controls[keys[0]].value} ${group.controls[keys[1]].value}`, 'YYYY-MM-DD HH:mm', true);
        if (!time.isValid()) return null;
        const currentTime = moment();
        if (currentTime.unix() > time.unix()) {
            return {afterCurrentTime: true};
        }
        return null;
    }
}
