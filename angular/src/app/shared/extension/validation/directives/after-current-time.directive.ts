import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, Validator, FormGroup } from '@angular/forms';

import { CustomValidators } from '../custom-validators';

const AFTER_CURRENT_TIME_VALIDATOR: any = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => AfterCurrentTimeValidator),
    multi: true
};

@Directive({
    selector: '[afterCurrentTime][ngModelGroup]',
    providers: [AFTER_CURRENT_TIME_VALIDATOR]
})
export class AfterCurrentTimeValidator implements Validator {
    validate(c: FormGroup): { [key: string]: any } {
        return CustomValidators.afterCurrentTime(c);
    }
}
