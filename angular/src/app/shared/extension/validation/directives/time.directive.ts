import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

import { CustomValidators } from "../custom-validators";

const TIME_VALIDATOR: any = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => TimeValidator),
    multi: true
};

@Directive({
    selector: '[time][formControlName],[time][formControl],[time][ngModel]',
    providers: [TIME_VALIDATOR]
})

export class TimeValidator implements Validator {
    validate(c: AbstractControl): { [key: string]: any } {
        return CustomValidators.time(c);
    }
}
