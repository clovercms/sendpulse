import { NgModule } from "@angular/core";

import { EmailValidator } from "./directives/email.directive";
import { EqualValidator } from "./directives/equal.directive";
import { EqualToValidator } from "./directives/equal-to.directive";
import { DateValidator } from './directives/date.directive';
import { TimeValidator } from './directives/time.directive';
import { AfterCurrentTimeValidator } from './directives/after-current-time.directive';

const CUSTOM_FORM_DIRECTIVES = [
    EmailValidator,
    EqualValidator,
    EqualToValidator,
    DateValidator,
    TimeValidator,
    AfterCurrentTimeValidator
];

@NgModule({
    declarations: [CUSTOM_FORM_DIRECTIVES],
    exports: [CUSTOM_FORM_DIRECTIVES],
})

export class ValidationModule {
}
