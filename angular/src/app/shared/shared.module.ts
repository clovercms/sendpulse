import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, Http } from '@angular/http';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';

import { HttpRequestService } from './services/http-request.service';

@NgModule({
    imports: [
        HttpModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, '', '.json'),
            deps: [Http]
        }),
        CommonModule
    ],
    declarations: [

    ],
    exports: [
        HttpModule,
        TranslateModule,
        CommonModule
    ],
    providers: [
        HttpRequestService
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: []
        };
    }
}
