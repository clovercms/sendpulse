import { Injectable } from '@angular/core';
import { Response, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { HttpRequestService } from './http-request.service';

export interface ICreate {
    email: string;
    password: string;
    confirmPassword: string;
}

export interface ILogin {
    email: string;
    password: string;
}

export interface IResponse {
    data: Array<any>;
    messages: Array<string>;
}

@Injectable()
export class UserService {

    /**
     *
     * @type {string}
     */
    public urlLogin: string = '/user/login';

    /**
     *
     * @type {string}
     */
    public urlLogout: string = '/user/logout';

    /**
     *
     * @type {string}
     */
    public urlCreate: string = '/user';

    /**
     *
     * @param {Http} http
     * @param {HttpRequestService} httpService
     */
    public constructor(private http: Http, private httpService: HttpRequestService) {

    }

    /**
     *
     * @param {ILogin} data
     */
    public login(data: ILogin) {
        return this.http.post(this.urlLogin, JSON.stringify(data)).toPromise()
            .then((response: Response): IResponse => response.json());
    }

    /**
     *
     */
    public logout() {
        return this.http.get(this.urlLogout).toPromise()
            .then((response: Response) => response.json());
    }

    /**
     *
     * @param {ICreate} data
     */
    public create(data: ICreate) {
        return this.http.post(this.urlCreate, JSON.stringify(data)).toPromise()
            .then((response: Response): IResponse => response.json());
    }
}