import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpRequestService } from './http-request.service';
import { IRequestDataOptions, IResponseDataList as IResponseList } from '../extension/dt-table/dt-table.service';

export interface ICreate {
    date: string;
    title: string;
    description: string;
}

export interface IEvent extends ICreate {
    date: string;
    title: string;
    description: string;
    status: number;
}

export interface IResponse {
    data: Array<any>;
    messages: Array<string>;
}

export interface IResponseDataList extends IResponseList {
    messages: Array<string>;
    data: {
        list: Array<IEvent>;
        count: number;
    }
}

@Injectable()
export class EventService {

    /**
     *
     * @type {string}
     */
    public urlList: string = '/event';

    /**
     *
     * @type {string}
     */
    public urlCreate: string = '/event';

    /**
     *
     * @type {string}
     */
    public urlUpdate: string = '/event/{id}';

    /**
     *
     * @type {string}
     */
    public urlComplete: string = '/event/{id}';

    /**
     *
     * @type {string}
     */
    public urlDestroy: string = '/event/{id}';

    /**
     *
     * @param {HttpRequestService} http
     */
    public constructor(private http: HttpRequestService) {

    }

    public list(options: IRequestDataOptions) {
        return this.http.get(this.urlList, {search: options.getQueryString()})
            .then((response: Response): IResponseDataList => response.json());
    }

    public create(data: ICreate) {
        return this.http.post(this.urlCreate, JSON.stringify(data))
            .then((response: Response): IResponse => response.json());
    }

    public update(id: number, data: ICreate) {
        return this.http.put(this.urlUpdate.replace('{id}', id.toString()), JSON.stringify(data))
            .then((response: Response): IResponse => response.json());
    }

    public complete(id: number) {
        return this.http.patch(this.urlComplete.replace('{id}', id.toString()), JSON.stringify({}))
            .then((response: Response): IResponse => response.json());
    }

    public destroy(id: number) {
        return this.http.delete(this.urlDestroy.replace('{id}', id.toString()))
            .then((response: Response): IResponse => response.json());
    }

}