import { Injectable } from '@angular/core';
import { RequestOptionsArgs, Response, Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HttpRequestService {

    /**
     *
     * @type {Headers}
     */
    private headers: Headers = new Headers({
        "Content-Type": "application/json",
        "Accept": "application/json"
    });

    /**
     *
     * @param {Router} router
     * @param {Http} http
     */
    constructor(private router: Router, private http: Http) {

    }

    /**
     *
     * @param {string} url
     * @param {RequestOptionsArgs} options
     * @return {Promise<T>}
     */
    public get(url: string, options?: RequestOptionsArgs) {
        return this.http.get(url, options)
            .toPromise()
            .then((response: any) => this.successHandler(response))
            .catch(error => this.failedHandler(error));
    }

    /**
     *
     * @param {string} url
     * @param {string} body
     * @param {RequestOptionsArgs} options
     * @return {Promise<T>}
     */
    public post(url: string, body: string, options?: RequestOptionsArgs) {
        options = options ? options : {headers: this.headers};
        return this.http.post(url, body, options)
            .toPromise()
            .then((response: any) => this.successHandler(response))
            .catch((error: any) => this.failedHandler(error));
    }

    /**
     *
     * @param {string} url
     * @param {string} body
     * @param {RequestOptionsArgs} options
     * @return {Promise<T>}
     */
    public put(url: string, body: string, options?: RequestOptionsArgs) {
        options = options ? options : {headers: this.headers};
        return this.http.put(url, body, options)
            .toPromise()
            .then((response: any) => this.successHandler(response))
            .catch((error: any) => this.failedHandler(error));
    }

    /**
     *
     * @param {string} url
     * @param {string} body
     * @param {RequestOptionsArgs} options
     * @return {Promise<T>}
     */
    public patch(url: string, body: string, options?: RequestOptionsArgs) {
        options = options ? options : {headers: this.headers};
        return this.http.patch(url, body, options)
            .toPromise()
            .then((response: any) => this.successHandler(response))
            .catch((error: any) => this.failedHandler(error));
    }

    /**
     *
     * @param {string} url
     * @param {RequestOptionsArgs} options
     * @return {Promise<T>}
     */
    public delete(url: string, options?: RequestOptionsArgs) {
        return this.http.delete(url, options)
            .toPromise()
            .then((response: Response) => this.successHandler(response))
            .catch((error: any) => this.failedHandler(error));
    }

    /**
     *
     * @param {any} response
     * @return {any}
     * @private
     */
    protected successHandler(response: Response): any {
        if (this.logout(response.status)) {
            return Promise.reject(response);
        }
        return response;
    }

    /**
     *
     * @param {any} error
     * @return {Promise<void>|Promise<T>}
     * @private
     */
    protected failedHandler(error: any): any {
        this.logout(error.status);
        return Promise.reject(error);
    }

    /**
     *
     * @param {any} status
     * @return {boolean}
     * @private
     */
    protected logout(status: any) {
        if (status == 401 || status == 403) {
            localStorage.clear();
            this.router.navigate(['/']);
            return true;
        }
        return false;
    }
}