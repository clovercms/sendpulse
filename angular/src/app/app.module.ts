import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './app.routes';
import { AppComponent } from './app.component';

import { SharedModule } from './shared/shared.module';

import { LoginModule } from './auth/login';
import { SignUpModule } from './auth/sign-up';
import { UserModule } from './user';

import { AuthUser } from './auth.user';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        SharedModule.forRoot(),
        LoginModule,
        SignUpModule,
        UserModule
    ],
    providers: [AuthUser],
    bootstrap: [AppComponent],
    exports: []
})

export class AppModule {
}