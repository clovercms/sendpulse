<?php

namespace Services;

use Sendpulse\Service;
use Sendpulse\Service\Paginate;

class Event extends Service
{
    const STATUS_ACTIVE = 1;

    const STATUS_COMPLETE = 2;

    public function list(array $data)
    {
        $paginate = Paginate::get($data,
            [
                'id' => 'id',
                'date' => 'date id',
                'title' => 'title id',
                'description' => 'description id',
                'created_at' => 'created_at id'
            ],
            [
                'date' => 'date',
                'title' => 'title',
                'description' => 'description',
                'created_at' => 'created_at',
            ],
            [
                'user' => ['name' => 'users_id', 'values' => true]
            ]);

        $sql = "SELECT SQL_CALC_FOUND_ROWS 
                  *
                FROM events
                WHERE id IS NOT NULL 
                {$paginate['search']} {$paginate['filter']} {$paginate['sort']} {$paginate['limit']}";


        $list = self::db()->getAll($sql, $paginate['bindings']);
        $count = self::db()->getCell('SELECT FOUND_ROWS()');

        return [
            'list' => $list,
            'count' => (int)$count
        ];
    }

    public function create($userId, $date, $title, $description)
    {
        $event = self::db()->dispense('events');
        $event->users_id = $userId;
        $event->date = $date;
        $event->title = $title;
        $event->description = $description;
        self::db()->store($event);

        return $event;
    }

    public function update($id, $userId, $date, $title, $description)
    {
        $event = $this->get($id, $userId);
        $event->users_id = $userId;
        $event->date = $date;
        $event->title = $title;
        $event->description = $description;
        self::db()->store($event);

        return $event;
    }

    public function complete($id, $userId)
    {
        $event = $this->get($id, $userId);
        $event->status = self::STATUS_COMPLETE;
        self::db()->store($event);

        return $event;
    }

    public function destroy($id, $userId)
    {
        $event = $this->get($id, $userId);
        self::db()->trash($event);
    }

    public function get($id, $userId)
    {
        return self::db()->findOne('events', 'id = :id and users_id = :user_id'
            , [':id' => $id, ':user_id' => $userId]);
    }
}