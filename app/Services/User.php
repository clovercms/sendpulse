<?php

namespace Services;

use Sendpulse\Http\Session;
use Sendpulse\Service;

class User extends Service
{
    public function login(string $email, string $password)
    {
        $user = $this->getFromEmail($email);
        return $user && password_verify($password, $user->password) ? $user : null;
    }

    public function logout()
    {
        Session::destroy();
    }

    public function auth($user = null)
    {
        if ($user) {
            Session::set('id', $user->id);

            return $user;
        }

        $userId = Session::get('id');

        return $userId ? self::db()->load('users', $userId) : false;
    }

    public function create(string $email, $password)
    {
        $user = self::db()->dispense('users');
        $user->email = $email;
        $user->password = password_hash($password, PASSWORD_BCRYPT);
        return self::db()->store($user);
    }

    public function getFromEmail($email)
    {
        return self::db()->findOne('users', 'email = :email', [':email' => $email]);
    }
}