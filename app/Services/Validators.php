<?php

namespace Services;

class Validators
{
    protected static function getResult($validator)
    {
        if ($validator->validate()) {
            return [false, $validator->data()];
        }

        $errors = [];

        foreach ($validator->errors() as $field) {
            foreach ($field as $error) {
                $errors[] = $error;
            }
        }
        return [$errors, $validator->data()];
    }

    public static function createUser($request)
    {
        $validator = $request->getParamsValidator();

        $validator->rule('required', ['email', 'password', 'confirmPassword'])
            ->message('{field} is required.')
            ->labels(['Email', 'Password', 'Confirm password']);
        $validator->rule('lengthMax', ['email', 'password', 'confirmPassword'], 64)
            ->message('{field} can not be longer than 64 characters.')
            ->labels(['Email', 'Password', 'Confirm password']);
        $validator->rule('equals', 'password', 'confirmPassword')
            ->message('Password does not match the confirm password.');
        $validator->rule('email', 'email')
            ->message('Incorrect email address.');

        return self::getResult($validator);
    }

    public static function loginUser($request)
    {
        $validator = $request->getParamsValidator();

        $validator->rule('required', ['email', 'password'])
            ->message('{field} is required.')
            ->labels(['Email', 'Password']);

        return self::getResult($validator);
    }

    public static function setEvent($request)
    {
        $validator = $request->getParamsValidator();

        $validator->rule('required', ['date', 'title', 'description'])
            ->message('{field} is required.')
            ->labels(['Date', 'Title', 'Description']);

        $validator->rule('date', 'date', 'Y-m-d H:i:s')
            ->label('Time')
            ->message('{field} is invalid.');

        $date = new \DateTime();

        $validator->rule('dateAfter', 'date', $date)
            ->label('Date')
            ->message("{field} must not be less than the current, {$date->format('Y-m-d H:i:s')}.");

        $validator->rule('lengthMax', 'title', 255)
            ->message('{field} can not be longer than 255 characters.')
            ->label('title');

        $validator->rule('lengthMax', 'description', 4000)
            ->message('{field} can not be longer than 4000 characters.')
            ->label('Description');

        return self::getResult($validator);
    }

    public static function paginate($request, $defaultSortField = 'id')
    {
        $validator = $request->getParamsValidator();

        $validator->rule('required', ['limit', 'offset', 'sortField', 'sortDirection', 'searchFields', 'searchText', 'filters']);
        $validator->rule('integer', ['limit', 'offset']);
        $validator->rule('min', 'limit', 0);
        $validator->rule('max', 'limit', 500);
        $validator->rule('min', 'offset', 0);
        $validator->rule('regex', 'sortField', '/^[a-z_]{1,}$/');
        $validator->rule('in', 'sortDirection', ['asc', 'desc']);
        $validator->rule('array', ['searchFields', 'filters']);
        $validator->rule('lengthMin', 'searchText', 1);

        $validator->validate();

        $limit = $validator->errors('limit') ? 10 : (int)$request->param('limit');
        $offset = $validator->errors('offset') ? 0 : (int)$request->param('offset');
        $sortField = $validator->errors('sortField') ? $defaultSortField : $request->param('sortField');
        $sortDirection = $validator->errors('sortDirection') ? 'desc' : $request->param('sortDirection');
        $searchFields = $validator->errors('searchFields') ? [] : $request->param('searchFields');
        $searchText = $validator->errors('searchText') ? '' : $request->param('searchText');
        $filters = $validator->errors('filters') ? [] : $request->param('filters');

        return [
            'limit' => $limit,
            'offset' => $offset,
            'sortField' => $sortField,
            'sortDirection' => $sortDirection,
            'searchFields' => $searchFields,
            'searchText' => $searchText,
            'filters' => $filters
        ];
    }
}