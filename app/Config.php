<?php

class Config
{
    const DEFAULT_TIMEZONE = 'Europe/Kiev';

    const SESSION_LIFETIME = 604800;

    const MYSQL_HOST = 'localhost';

    const MYSQL_PORT = 3306;

    const MYSQL_USER = 'root';

    const MYSQL_PASSWORD = 'misteriya';

    const MYSQL_DATABASE = 'sendpulse';

    const LOG_FILE = 'default.log';

}