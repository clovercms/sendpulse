<?php

namespace Config;


class System
{
    const DEFAULT_TIMEZONE = 'Europe/Kiev';

    const SESSION_LIFETIME = 604800;

    const MYSQL_HOST = 'todoapp_mysql';

    const MYSQL_PORT = 3306;

    const MYSQL_USER = 'root';

    const MYSQL_PASSWORD = 'password';

    const MYSQL_DATABASE = 'todoapp';
}