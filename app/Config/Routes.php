<?php

namespace Config;

use Sendpulse\Http\Route;
use Sendpulse\Http\Route\AbstractConfig;

class Routes extends AbstractConfig
{
    public function init()
    {
        Route::namespace('Controllers', function () {

            Route::get('', 'IndexController@index');
            Route::get('*', 'IndexController@index');

            Route::post('user', 'UserController@create');
            Route::post('user/login', 'UserController@login');
            Route::get('user/logout', 'UserController@logout');

            Route::get('event', 'EventController@index');
            Route::post('event', 'EventController@create');
            Route::get('event/{id}', 'EventController@show', ['id' => '[1-9]\d*']);
            Route::put('event/{id}', 'EventController@update');
            Route::patch('event/{id}', 'EventController@complete');
            Route::delete('event/{id}', 'EventController@destroy');
        });
    }
}