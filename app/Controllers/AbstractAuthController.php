<?php

namespace Controllers;

use Sendpulse\AbstractController;
use Sendpulse\Http\Response;
use Sendpulse\Http\StatusCode;
use Services\User;

class AbstractAuthController extends AbstractController
{
    protected $access = [];

    public $user;

    public function __invoke(string $method, array $arguments)
    {
        if (in_array($method, $this->access)) {

            $user = new User();
            $this->user = $user->auth();

            if (!$this->user) {

                return Response::status(StatusCode::UNAUTHORIZED);
            }
        }

        return $this->run($method, $arguments);
    }
}