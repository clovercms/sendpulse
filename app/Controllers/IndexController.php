<?php

namespace Controllers;

use Sendpulse\Http\StatusCode;
use Sendpulse\Http\Request;
use Sendpulse\Http\Response;

class IndexController extends AbstractAuthController
{
    public function index(Request $request)
    {
        if ($request->isAjax()) {
            return Response::status(StatusCode::NOT_FOUND);
        }

        ob_start();
        ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="/">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>TODO APP</title>
        <link href="/vendor.css" rel="stylesheet">
        <link href="/custom.css" rel="stylesheet">
    </head>
    <body>
        <app-root>
            <h4 class="text-center text-primary">Loading...</h4>
        </app-root>
        <script src="/vendor.ext.js"></script>
        <script src="/polyfills.js"></script>
        <script src="/vendor.js"></script>
        <script src="/app.js"></script>
    </body>
</html>
        <?php
        return Response::html(ob_get_clean());
    }
}