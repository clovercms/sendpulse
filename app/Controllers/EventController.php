<?php

namespace Controllers;

use Sendpulse\Http\Request;
use Sendpulse\Http\Response;
use Sendpulse\Http\StatusCode;
use Services\Event;
use Services\Validators;

class EventController extends AbstractAuthController
{
    protected $access = ['index', 'create', 'show', 'update', 'complete', 'destroy'];

    public function index(Request $request, Event $event)
    {
        $data = Validators::paginate($request);
        $data['filters'] = [
            ['name' => 'user', 'value' => $this->user->id]
        ];

        return Response::json($event->list($data), 'Operation Success.');
    }

    public function create(Request $request, Event $event)
    {
        list($errors, $data) = Validators::setEvent($request);

        if ($errors) {
            return Response::status($errors, StatusCode::BAD_REQUEST);
        }

        $event->create($this->user->id, $data['date'], $data['title'], $data['description']);

        return Response::status();
    }

    public function update(Request $request, Event $event)
    {
        list($errors, $data) = Validators::setEvent($request);

        if ($errors) {
            return Response::status($errors, StatusCode::BAD_REQUEST);
        }

        $event->update(
            $request->option('id')
            , $this->user->id
            , $data['date']
            , $data['title']
            , $data['description']);

        return Response::status();
    }

    public function complete(Request $request, Event $event)
    {
        $event->complete($request->option('id'), $this->user->id);

        return Response::status();
    }

    public function destroy(Request $request, Event $event)
    {
        $event->destroy($request->option('id'), $this->user->id);

        return Response::status();
    }
}