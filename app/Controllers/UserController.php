<?php

namespace Controllers;

use Sendpulse\Http\Request;
use Sendpulse\Http\Response;
use Sendpulse\Http\StatusCode;
use Services\User;
use Services\Validators;

class UserController extends AbstractAuthController
{
    public function create(Request $request, User $user)
    {
        list($errors, $data) = Validators::createUser($request);

        if ($errors) {

            return Response::json($errors, StatusCode::BAD_REQUEST);
        }

        if ($user->getFromEmail($data['email'])) {
            $message = 'A user with the email address that you specified already exists.';

            return Response::status($message, StatusCode::BAD_REQUEST);
        }

        $user->create($data['email'], $data['password']);

        return Response::status();
    }

    public function login(Request $request, User $user)
    {
        list($errors, $data) = Validators::loginUser($request);

        if ($errors) {

            return Response::status(StatusCode::BAD_REQUEST);
        }

        $bean = $user->login($data['email'], $data['password']);

        if (!$user->auth($bean)) {

            return Response::status('Operation Failed.', StatusCode::BAD_REQUEST);
        }

        return Response::status();
    }

    public function logout(User $user)
    {
        $user->logout();

        return Response::status();
    }
}