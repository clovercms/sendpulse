<?php

namespace Sendpulse\Service;

class Paginate
{
    /**
     *
     * @param array $data
     * @param array $sortFields
     * @param array $searchFields
     * @param array $filterFields
     * @return array
     */
    public static function get(array $data, array $sortFields = [], array $searchFields = [], array $filterFields = [])
    {
        $bindings = [];
        return [
            'limit' => self::getLimit($data, $bindings),
            'sort' => self::getSort($sortFields, $data),
            'search' => self::getSearch($searchFields, $data, $bindings),
            'filter' => self::getFilter($filterFields, $data, $bindings),
            'bindings' => $bindings
        ];
    }

    /**
     *
     * @param array $data ['limit' => int, 'offset' => int]
     * @param array $bindings
     * @return string
     */
    public static function getLimit($data, array &$bindings)
    {
        if (isset($data['limit'], $data['offset']) &&
            intval($data['limit']) > 0 && intval($data['offset']) >= 0
        ) {
            $bindings[':limit'] = $data['limit'];
            $bindings[':offset'] = $data['offset'];

            return 'LIMIT :offset, :limit';
        }

        return '';
    }

    /**
     *
     * @param array $sortFields ['name' => 'table_col_1 table_col_2 desc', ...]
     * @param array $data ['sortField' => string, 'sortDirection' => 'asc/desc']
     * @return string
     */
    public static function getSort($sortFields, $data)
    {
        if (isset($data['sortField'], $data['sortDirection'])
            && array_key_exists($data['sortField'], $sortFields)
            && in_array(strtoupper($data['sortDirection']), ['ASC', 'DESC'])
        ) {

            $sql = 'ORDER BY ';
            $fields = explode(' ', preg_replace('#\s{1,}#ui', ' ', $sortFields[$data['sortField']]));

            foreach ($fields as $key => $field) {

                $sortDirection = isset($fields[$key + 1]) &&
                in_array(strtoupper($fields[$key + 1]), array('ASC', 'DESC')) ?
                    strtoupper($fields[$key + 1]) : strtoupper($data['sortDirection']);

                if (!in_array(strtoupper($field), array('ASC', 'DESC'))) {

                    $sql .= ($key > 0 ? ', ' : '') . $field . ' ' . $sortDirection;
                }
            }

            return $sql;
        }

        return '';
    }

    /**
     *
     * @param array $searchFields
     * @param array $data ['searchText' => string, 'searchFields' => string[]]
     * @param string[] $bindings
     * @return string
     */
    public static function getSearch(array $searchFields, array $data, array &$bindings)
    {
        if (isset($data['searchFields'], $data['searchText'])
            && is_array($data['searchFields']) && is_string($data['searchText'])
            && trim($data['searchText']) !== ''
        ) {

            $search = [];
            $key = 0;

            foreach ($data['searchFields'] as $field) {

                ++$key;

                if (is_string($field) && array_key_exists($field, $searchFields)) {

                    $search[] = self::getConcatWsField($searchFields[$field]) . " LIKE :search_{$key}";
                    $bindings[":search_{$key}"] = "%{$data['searchText']}%";
                }
            }

            if (count($search) > 0) {

                return 'AND (' . implode(' OR ', $search) . ')';
            }
        }

        return '';
    }

    /**
     *
     * @param string $field
     * @param string $fieldSeparate
     * @return string
     */
    private static function getConcatWsField($field, $fieldSeparate = ',')
    {
        $fields = explode($fieldSeparate, $field);
        $count = count($fields);

        if ($count > 1) {

            $sql = 'CONCAT(';

            foreach ($fields as $key => $item) {

                $sql .= $item . ($key < ($count - 1) ? ", ' ', " : "");
            }
            $sql .= ')';
        } else {

            $sql = $field;
        }

        return $sql;
    }

    /**
     *
     * @param array $filterFields
     * @param string[] $data []
     * @param string[] $bindings ['key' => 'value']
     * @return string
     */
    public static function getFilter(array $filterFields, array $data, array &$bindings)
    {
        $key = 0;
        $parts = [];

        foreach ($data['filters'] as $filter) {

            $filter = (array)$filter;
            ++$key;

            if (isset($filter['name'], $filter['value']) &&
                array_key_exists($filter['name'], $filterFields) &&
                ($filterFields[$filter['name']]['values'] === true ||
                    in_array($filter['value'], $filterFields[$filter['name']]['values']))
            ) {
                $filterName = $filterFields[$filter['name']]['name'];
                $filterValue = $filter['value'];
                $parts[] = "{$filterName} = :filter_{$key}";
                $bindings[":filter_{$key}"] = $filterValue;
            }
        }

        return (count($parts) > 0) ? 'AND (' . implode(' AND ', $parts) . ')' : '';
    }

}