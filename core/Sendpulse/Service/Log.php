<?php

namespace Sendpulse\Service;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log
{
    protected static $logger;

    public static function init($filename)
    {
        if (!self::$logger) {
            self::$logger = new Logger('name');
            self::$logger->pushHandler(new StreamHandler($filename, Logger::ERROR));
            self::$logger->error();
        }
    }

    public static function send()
    {
        return self::$logger;
    }
}