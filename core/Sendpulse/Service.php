<?php

namespace Sendpulse;

use RedBeanPHP\R;

class Service
{
    /**
     * @var R
     */
    protected static $db;

    public static function db()
    {
        if (!self::$db) {

            self::$db = new R();
        }

        return self::$db;
    }
}