<?php

namespace Sendpulse;

use Sendpulse\Http\Router;
use Sendpulse\Http\Route;
use Sendpulse\Http\Route\Section\Statical;
use Sendpulse\Http\Route\Registry;
use Sendpulse\Http\Request;
use Config\Routes;

class App
{
    private $response;

    public function __construct(Request $request)
    {
        $this->routesInitialize();
        $this->response = Router::run($request);
    }

    public static function run($request)
    {
        return new self($request);
    }

    protected function routesInitialize()
    {
        Route::setRootSection(new Statical(new Registry()), Routes::class);
    }

    public function __destruct()
    {
        $this->response->run();
    }

}