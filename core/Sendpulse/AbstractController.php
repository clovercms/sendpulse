<?php

namespace Sendpulse;

class AbstractController
{
    protected function run(string $method, array $arguments)
    {
        return $this->{$method}(...$arguments);
    }
}