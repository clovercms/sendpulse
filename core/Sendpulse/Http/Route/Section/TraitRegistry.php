<?php

namespace Sendpulse\Http\Route\Section;

use Sendpulse\Http\Route\AbstractRegistry;
use Sendpulse\Http\Route\Exception;

trait TraitRegistry
{
    protected $registry;

    public function __construct($registry)
    {
        $this->setRegistry($registry);
    }

    public function setRegistry($registry)
    {
        if ($this->registry) {
            throw new Exception("Route registry is already set!");
        }

        $this->registry = $registry;
    }

    public function getRegistry(): AbstractRegistry
    {
        return $this->registry;
    }
}