<?php

namespace Sendpulse\Http\Route\Section;

use Sendpulse\Http\Route\AbstractSection;
use Sendpulse\Http\Route\Exception;

class Optional extends AbstractSection
{
    use TraitRegistry;

    protected $notRequired = [];

    protected $match;

    public function setName(string $name)
    {
        if (!$this->name) {
            parent::setName($name);
        } elseif ($this->name && $this->name !== $name) {
            throw new Exception("Route optional section name is already set!");
        }
    }

    public function setNotRequired(string $method)
    {
        if (in_array($method, $this->notRequired)) {
            throw new Exception("Route optional section not required method ({$method}) is already set!");
        }

        $this->notRequired[] = $method;
    }

    public function setMatch(string $match)
    {
        if ($this->match) {
            throw new Exception("Route optional section match is already set!");
        }

        $this->match = $match;
    }

    public function getMatch()
    {
        return $this->match;
    }
}