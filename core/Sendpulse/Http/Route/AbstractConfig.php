<?php

namespace Sendpulse\Http\Route;

abstract class AbstractConfig
{
    abstract public function init();
}