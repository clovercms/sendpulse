<?php

namespace Sendpulse\Http\Route;

abstract class AbstractSection implements InterfaceSection
{
    protected $name;

    protected $actions = [];

    protected $parent;

    public function setName(string $name)
    {
        if ($this->name) {
            throw new Exception("Route section name is already set!");
        }
        $this->name = $name;
    }

    public function setAction(string $method, string $action)
    {
        if (!isset($this->actions[$method])) {
            $this->actions[$method] = $action;
        } else {
            throw new Exception("Route section action is already set!");
        }
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    public function getName(): string
    {
        if (!$this->name) {
            throw new Exception("Route section name is not set!");
        }

        return $this->name;
    }

    public function getAction($method)
    {
        return $this->actions[$method] ?? null;
    }

    public function getParent()
    {
        return $this->parent;
    }
}