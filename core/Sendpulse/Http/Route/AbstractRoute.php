<?php

namespace Sendpulse\Http\Route;

abstract class AbstractRoute
{
    protected static $methods = ['post', 'get', 'put', 'patch', 'delete'];

    protected static $rootSection;

    protected static $namespace = '';

    public static function __callStatic($name, $arguments)
    {
        $method = in_array($name, self::$methods) ? $name : false;
        if ($method) {
            static::add($method, $arguments[0], $arguments[1], $arguments[2] ?? []);
        }
    }

    abstract public static function isAllowedMethod(string $method): bool;

    abstract public static function setRootSection(AbstractSection $section, $config);

    abstract public static function getRootSection(): AbstractSection;

    abstract public static function namespace(string $namespace, callable $function);

    abstract protected static function add(string $method, string $uriPath, string $action, array $matches);
}