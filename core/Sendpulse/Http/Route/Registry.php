<?php

namespace Sendpulse\Http\Route;

class Registry extends AbstractRegistry
{
    protected $list = [];

    protected $optional;

    protected $any;

    public function add($section)
    {
        if (!isset($this->list[$section->getName()])) {
            $this->list[$section->getName()] = $section;
        }
    }

    public function setOptional($section)
    {
        if (!$this->optional) {
            $this->optional = $section;
        } else {
            throw new Exception("Set route optional!");
        }
    }

    public function setAny($section)
    {
        if (!$this->any) {
            $this->any = $section;
        } else {
            throw new Exception("Set route optional!");
        }
    }

    public function get($name)
    {
        return isset($this->list[$name]) ? $this->list[$name] : null;
    }

    public function getOptional()
    {
        return $this->optional;
    }

    public function getAny()
    {
        return $this->any;
    }
}