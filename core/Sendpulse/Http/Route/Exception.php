<?php

namespace Sendpulse\Http\Route;

class Exception extends \Exception
{
    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct('Failed: ' . $message, $code, $previous);
    }
}