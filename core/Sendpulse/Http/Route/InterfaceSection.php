<?php

namespace Sendpulse\Http\Route;

interface InterfaceSection
{
    public function setName(string $name);

    public function setAction(string $method, string $action);

    public function setParent($parent);

    public function getName(): string;

    public function getAction($method);

    public function getParent();
}