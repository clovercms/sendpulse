<?php

namespace Sendpulse\Http\Route;

abstract class AbstractRegistry
{
    abstract public function add($section);

    abstract public function setOptional($section);

    abstract public function setAny($section);

    abstract public function get($name);

    abstract public function getOptional();

    abstract public function getAny();
}