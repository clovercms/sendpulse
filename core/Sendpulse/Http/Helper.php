<?php

namespace Sendpulse\Http;


class Helper
{
    static function uriPathToSections($uriPath)
    {
        $sections = explode('/', $uriPath);

        return array_filter($sections, function ($item) {
            return !!$item;
        });
    }
}