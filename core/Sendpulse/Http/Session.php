<?php

namespace Sendpulse\Http;


class Session
{
    private static $session;

    public static function init($lifeTime, $data = [])
    {
        if (!self::$session && $data) {
            self::$session = $data;
        } elseif (!self::$session) {
            session_set_cookie_params($lifeTime);
            ini_set('session.cookie_lifetime', $lifeTime);
            ini_set('session.gc_maxlifetime', $lifeTime);
            session_start();
            self::$session = &$_SESSION;
        }
    }

    public static function set($key, $value)
    {
        self::$session[$key] = $value;
    }

    public static function get($key)
    {
        return self::$session[$key] ?? null;
    }

    public static function remove($key)
    {
        if (isset(self::$session[$key])) {

            unset(self::$session[$key]);
        }
    }

    public static function destroy()
    {
        session_destroy();
    }
}