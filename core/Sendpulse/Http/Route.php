<?php

namespace Sendpulse\Http;

use Sendpulse\Http\Route\AbstractRoute;
use Sendpulse\Http\Route\AbstractSection;
use Sendpulse\Http\Route\Exception;
use Sendpulse\Http\Route\Section\Any;
use Sendpulse\Http\Route\Section\Optional;
use Sendpulse\Http\Route\Registry;
use Sendpulse\Http\Route\Section\Statical;

class Route extends AbstractRoute
{
    public static function isAllowedMethod(string $method): bool
    {
        return in_array(strtolower($method), self::$methods);
    }

    public static function setRootSection(AbstractSection $section, $config)
    {
        if (!self::$rootSection) {
            self::$rootSection = $section;
            (new $config)->init();
        }
    }

    public static function getRootSection(): AbstractSection
    {
        return self::$rootSection;
    }

    public static function namespace(string $namespace, callable $function)
    {
        self::$namespace = $namespace . '\\';
        $function();
        self::$namespace = '';
    }

    protected static function setStaticSection($name, $method, &$parentSection, $action = null)
    {
        $currentRouteRegistry = $parentSection->getRegistry();
        $currentSection = $currentRouteRegistry->get($name);

        if (!$currentSection) {
            $currentSection = new Statical(new Registry());
            $currentSection->setParent($parentSection);
            $currentSection->setName($name);
            $currentRouteRegistry->add($currentSection);
        }

        if ($action) {
            $currentSection->setAction($method, static::$namespace . $action);
        }

        $parentSection = $currentSection;
    }

    protected static function setOptionalSection($name, $method, &$parentSection, $action, $required, $matches)
    {
        $currentRouteRegistry = $parentSection->getRegistry();
        $currentSection = $currentRouteRegistry->getOptional();

        if (!$currentSection) {
            $currentSection = new Optional(new Registry());
            $currentSection->setParent($parentSection);

            if (!isset($matches[$name])) {
                throw new Exception("Not match by {$name}");
            }

            $currentSection->setName($name);
            $currentSection->setMatch($matches[$name]);
            $currentRouteRegistry->setOptional($currentSection);
        }

        $currentSection->setName($name);

        if ($action) {
            $currentSection->setAction($method, static::$namespace . $action);

            if (!$required) {
                $currentSection->setNotRequired($method);
            }
        }

        $parentSection = $currentSection;
    }

    protected static function setAnySection($name, $method, &$parentSection, $action)
    {
        $currentRouteRegistry = $parentSection->getRegistry();
        $currentSection = $currentRouteRegistry->getAny();

        if (!$currentSection) {
            $currentSection = new Any(new Registry());
            $currentSection->setParent($parentSection);

            $currentSection->setName($name);
            $currentRouteRegistry->setAny($currentSection);
        }

        if ($action) {
            $currentSection->setAction($method, static::$namespace . $action);
        }

        $parentSection = $currentSection;
    }

    protected static function optionalSection($name)
    {
        $match = [];
        preg_match('/^\{(?P<name>[a-zA-Z_]+)(?P<required>\??)\}$/', $name, $match);
        $optional = [$match['name'] ?? null, !($match['required'] ?? false)];

        return $optional;
    }

    protected static function checkAny($name)
    {
        return $name === '*';
    }

    protected static function add(string $method, string $uriPath, string $action, array $matches)
    {
        $parentSection = static::$rootSection;
        $sections = Helper::uriPathToSections($uriPath);

        if (!$sections) { // set root action
            $parentSection->setName('root');
            $parentSection->setAction($method, static::$namespace . $action);
        }

        $last = count($sections) - 1;

        foreach ($sections as $key => $section) {
            list($optional, $required) = self::optionalSection($section);
            $name = $optional ? $optional : $section;
            $classMethod = $key === $last ? $action : null;

            if ($optional) {
                self::setOptionalSection($name, $method, $parentSection, $classMethod, $required, $matches);
            } elseif (self::checkAny($section)) {
                self::setAnySection($name, $method, $parentSection, $classMethod);
            } else {
                self::setStaticSection($name, $method, $parentSection, $classMethod);
            }
        }
    }
}