<?php

namespace Sendpulse\Http;

use Sendpulse\Http\Route\AbstractSection;
use Sendpulse\Http\Route\Section\Optional;
use Sendpulse\Http\Route\Section\Any;

class Router
{
    protected static $anySection;

    public static function run(Request $request)
    {
        if (Route::isAllowedMethod($request->getMethod())) {

            $section = Route::getRootSection();
            $method = strtolower($request->getMethod());
            $parts = Helper::uriPathToSections($request->getRequestPath());
            list($action, $params) = array_values(self::getAction($section, $method, $parts));
            $request->setOptions($params);

            return $action ? self::runAction($action, $request) :
                Response::json([], StatusCode::NOT_FOUND);
        }

        return Response::json([], StatusCode::METHOD_NOT_ALLOWED);
    }

    protected static function runAction($action, $request)
    {
        list($className, $methodName) = explode('@', $action);
        $class = new \ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $params = $method->getParameters();
        $arguments = [];

        foreach ($params as $param) {

            $argumentClass = $param->getClass()->getName();
            $arguments[] = $argumentClass === Request::class ? $request : new $argumentClass;
        }

        $objController = new $className;

        return $objController($methodName, $arguments);
    }

    protected static function getChildSection($section, $name)
    {
        $registry = $section->getRegistry();
        return $registry->get($name) ?? $registry->getOptional() ?? $registry->getAny();
    }

    protected static function getAction(AbstractSection $section, $method, $parts, $params = [])
    {
        $action = ['action' => null, 'params' => $params];

        if (!$parts) {

            $action['action'] = $section->getAction($method);
        } elseif (!$section instanceof Any) {

            $name = array_shift($parts);

            $childSection = self::getChildSection($section, $name);
            $anySection = $section->getRegistry()->getAny();
            self::$anySection = $anySection ? $anySection : self::$anySection;

            if ($childSection) {

                if ($childSection instanceof Optional) {

                    if (!preg_match("/^{$childSection->getMatch()}$/", $name)) {

                        return [
                            'action' => self::$anySection ? self::$anySection->getAction($method) : null,
                            'params' => $params
                        ];
                    }

                    $params[$childSection->getName()] = $name;
                }

                $action = self::getAction($childSection, $method, $parts, $params);
            }
        }

        $action['action'] = !$action['action'] && self::$anySection ? self::$anySection->getAction($method) : $action['action'];

        return $action;
    }
}