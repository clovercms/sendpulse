<?php

namespace Sendpulse\Http;

class Response
{
    protected $data;

    protected $headers = [];

    protected $status;

    public function __construct(string $data, $headers = [], $status = StatusCode::OK)
    {
        $this->data = $data;
        $this->headers = $headers;
        $this->status = $status;
    }

    public function run()
    {
        http_response_code($this->status);
        foreach ($this->headers as &$header) {
            header($header);
        }
        echo $this->data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public static function html($data, $status = StatusCode::OK)
    {
        return new Response($data, ['Content-Type: text/html; charset=utf-8'], $status);
    }

    public static function json(array $data, ...$params)
    {
        $body = ['data' => $data, 'messages' => []];
        $status = isset($params[1]) && is_int($params[1]) ? $params[1] : StatusCode::OK;

        if (isset($params[0])) {
            $status = is_int($params[0]) ? $params[0] : $status;
            $body['messages'] = is_string($params[0]) ? [$params[0]] :
                (is_array($params[0]) ? $params[0] : $body['messages']);
        }

        return new Response(json_encode($body), ['Content-Type: application/json; charset=utf-8'], $status);
    }

    public static function status(...$params)
    {
        return self::json([], ...$params);
    }
}