<?php

namespace Sendpulse\Http;

use Valitron\Validator;

class Request
{
    protected $method;

    protected $requestPath;

    protected $options = [];

    protected $params = [];

    protected $isAjax;

    public function __construct($options = [], $params = [])
    {
        $this->method = $_SERVER['REQUEST_METHOD'] ?? null;
        $this->requestPath = parse_url($_SERVER['REQUEST_URI'] ?? '')['path'];
        $input = json_decode(file_get_contents("php://input"), true) ?? [];
        $this->options = $options;
        $this->params = array_merge($_REQUEST, $input);
        $this->isAjax = strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] ?? '') === 'xmlhttprequest';
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    public function getRequestPath()
    {
        return $this->requestPath;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function option($name, $default = null)
    {
        return $this->options[$name] ?? $default;
    }

    public function options()
    {
        return $this->options;
    }

    public function param($name, $default = null)
    {
        return $this->params[$name] ?? $default;
    }

    public function params()
    {
        return $this->params;
    }

    public function isAjax()
    {
        return $this->isAjax;
    }

    public function getParamsValidator()
    {
        return new Validator($this->params());
    }
}