<?php

namespace Sendpulse\Http;

class StatusCode
{
    const OK = 200;

    const BAD_REQUEST = 400;

    const UNAUTHORIZED = 401;

    const NOT_FOUND = 404;

    const METHOD_NOT_ALLOWED = 405;

    // etc ...
}